**<h1>Application Vizsources - Documentation technique.</h1>**

- [Objectif de l'application](#objectif-de-lapplication)
- [Guide de déploiement rapide](#guide-de-déploiement-rapide)
- [Installation](#installation)
- [Configuration du build de l’application](#configuration-du-build-de-lapplication)
  - [A installer au départ](#a-installer-au-départ)
  - [Node.js et paquets à installer](#nodejs-et-paquets-à-installer)
  - [Build du code frontend et lancement](#build-du-code-frontend-et-lancement)
- [Internationalisation](#internationalisation)

L'application Web VizSources (le logiciel) est sous copyright : © the [ANR Portic Project](https://anr.portic.fr/) 2019-2023. All rights reserved.  
Le code source est publié en open-source sous licence [AGPL v3](docs/agpl-3.0.md) .

# Objectif de l'application

Application pour visualiser les sources de Navigo :  

- emplacements des ports français et leur appartenance aux : Provinces, amirautés, Fermes du XVIIIème siècle, 
- complétude de divers paramètres (ports d'attache, tonnages, produits ...).

L'application se connecte à l'API [porticapi](http://data.portic.fr) pour obtenir les données affichées.

L'application est accessible en ligne à l'adresse : [http://vizsources.portic.fr](http://vizsources.portic.fr)

# Guide de déploiement rapide

Rappel : pour mettre l'application Web en production, 

- un serveur Web compatible WSGI est nécessaire (ex.: Apache avec mod_wsgi), car l'application utilise le paquet / micro-framework *Flask* (écrit en Python)
- il faut Git, 
- un accès (HumanID + accès Gitlab) au serveur de dépôts de sources [gitlab.huma-num.fr](https://gitlab.huma-num.fr/),
- avoir déjà clôné le dépôt [Portic / vizsources](https://gitlab.huma-num.fr/portic/vizsources), 
- il faut une version stable récente de Node.js, 
- Yarn v1.x
- une version de Python (et un environnement virtuel activé).

Pour mettre à jour l'application en production sur le serveur Web, dans le dossier des sources de l'application, effectuer les opérations suivantes :

- récupérer la dernière version des sources de l'application dans la branche **main** (non-standard) :  
  `git pull`
- installer les modules Node nécessaires :  
  `yarn install`
- uniquement si demandé (pour résoudre des problèmes de compatibilité entre paquets) - mettre à jour les paquets npm, vers la version la plus récente :  
  `yarn upgrade`
- construire l'application React pour la production :  
  `yarn run build`
- Python doit être installé et accessible en ligne de commande (s'en assurer)
- installer les paquets Python nécessaires (ou ajouter les nouveaux paquets si nécessaire) :  
  `pip install -r requirements.txt`
- construire le site du manuel utilisateur (lien 'Aide'), qui apparaît alors dans le dossier [static/manual](static/manual) :  
  `mkdocs build`  
  Note : Pendant la mise au point du / des fichier(s) source de la documentation en Markdown (.md), un serveur de développement permet de pré-visualiser instantanément le site d'aide de l'application (sans déployer complètement) avec mise à jour automatique dans le navigateur à chaque modification d'un fichier. Commande pour construire le site du manuel utilisateur et lancer le serveur :  
  `mkdocs serve`

# Installation

La présente documentation technique (pour développeur travaillant sous Windows 10) explique la construction de l’application React (jsx) / Flask (python) ‘vizsources’. Cette application permet de visualiser les sources (G5, santé, petit cabotage, ...) de la base de données Portic. L’application est connectée à l’API REST ‘porticapi’ (qui a son propre dépôt Gitlab).

Il faut donc d’abord installer sur la machine de développement dont vous devez être administrateur local, le serveur de base de données PostgreSQL avec l’extension PostGIS, et restaurer la sauvegarde la plus récente de la base de données Portic (voir document séparé décrivant ces opérations). Puis clôner le dépôt de l’application ‘porticapi’ et lancer le serveur Web de cette API REST, et s’assurer de son bon fonctionnement (nécessaire pour cette application).


# Configuration du build de l’application

Actuellement c’est une application Python / Flask minimale qui sert les fichiers (page d’accueil index.html et code JavaScript / React minifié) de l’application React. Le code frontend (JavaScript et React côté navigateur) utilise de nombreux modules Node.js installés par Yarn (alternative à NPM). Le build est fait par Webpack.


## A installer au départ

Il est nécessaire de disposer de son adresse e-mail professionnelle (par exemple de la forme prenom.nom@univ-poitiers.fr) . 

Il faut d’abord se créer un humanid (inscription sur https://www.huma-num.fr ), pour accéder à tous les services de Huma-Num. 

Puis aller sur le site des dépôts Gitlab de Huma-Num : https://gitlab.huma-num.fr, et demander par mail la création d’un identifiant pour utiliser le service Gitlab. Cette opération peut demander une demi-journée.

Christine Plumejeaud-Perreau peut alors accorder des droits d’accès aux dépôts des sources du projet Portic : au minimum le projet ‘porticapi’ et le projet ‘vizsources’.

GIT doit être installé en ligne de commande pour Windows 10, et disponible dans le PATH. Il faut configurer GIT pour s’identifier avec son nom (ex. : Prénom Nom) et son e-mail professionnel (ex. : prenom.nom@univ-poitiers.fr) en tapant d’abord les deux commandes GIT suivantes :


```
git config --global user.name "Prénom Nom"
git config --global user.email "prenom.nom@univ-poitiers.fr"
```

Il faut clôner avec GIT (manuellement en ligne de commande la première fois) le code du dépôt Gitlab ‘vizsources’ présent sur le serveur gitlab.huma-num.fr dans le groupe de projets ‘Portic’ : https://gitlab.huma-num.fr/portic . Le dépôt ‘vizsources’ et le groupe ne sont visibles que si des droits d’accès vous ont été accordés sur le dépôt. Il faut alors créer sur le site https://gitlab.huma-num.fr un ‘Access Token’ qui servira de mot de passe pour pouvoir clôner le dépôt https://gitlab.huma-num.fr/portic/vizsources en https avec GIT : 

`git clone https://gitlab.huma-num.fr/portic/vizsources` 

Lancer cette commande en ligne de commandes (cmd) permet à Windows 10 de mémoriser la paire : identifiant / mot de passe, qui seront automatiquement envoyés au serveur Gitlab par Windows 10 les fois suivantes :
l’identifiant (sans le @) est le nom d’utilisateur Gitlab qui vous a été envoyé par mail,
le mot de passe est l’Access Token créé sur le site Web https://gitlab.huma-num.fr . Il faut le sauvegarder dans un fichier .txt .

L’application Flask minimale de vizsources a été développée avec Python 3.9(.9) qu’il faut installer et mettre dans le PATH. Cette version de Python permet aussi de :  
- installer le serveur de bases de données PostgreSQL 14(.1), qui nécessite Python 3.9 pour faire fonctionner l’extension PL/Python3u (voir notes d’installation du serveur PostgreSQL)
- installer l’extension PostGIS 3.1.4 (qui sera associée à la base de données)
- créer l’extension PL/Python3u directement sans problème (extension nécessaire pour les procédures stockées dans la base, dont certaines sont écrites en Python)
- installer l’outil client pour faire des requêtes SQL : DBeaver (nécessite Java OpenJDK en version supérieure à 11 – ici version 17 installée au préalable)
- créer l’utilisateur PostgreSQL navigo, créer la base portic_v6, associer les extensions nécessaires à la base
- restaurer la dernière sauvegarde de la base de données Portic en ligne de commande avec la commande psql (base volumineuse)
- s’assurer à l’aide de DBeaver que tous les schémas, types, fonctions, ... sont présents dans la base restaurée. En effet, si certaines extensions n’ont pas été correctement installées et associées à la base de données Portic, les éléments de la base qui les utilisent seront manquants (non-restaurés) et l’API REST ne fonctionnera pas
- donner les droits d’accès aux utilisateurs PostgreSQL créés, sur les éléments de la base de données Portic
- installer, lancer le serveur Flask local de l’API REST ‘porticapi’ qui fournit les données au programme. Il faut s’assurer de son bon fonctionnement (tester d’abord l’API avant l’application).  

(voir document séparé décrivant l’installation préalable de la base de données).

J’utilise l’environnement de développement Visual Studio Code (open-source). Il est recommandé de faire de même, et d’installer dans VSCode l’extension ‘Python’ de Microsoft (la première - la plus utilisée). Prendre le temps de se familiariser avec, et de faire les tutos VSCode d’utilisation de Python et Flask pour gagner du temps par la suite.

Lors du développement (et donc à l’utilisation aussi) on utilise un environnement virtuel qui n’est pas sauvegardé dans le dépôt GIT. Il faut le recréer manuellement, l'activer, et installer manuellement les extensions Python nécessaires.

Ouvrir avec VSCode le dossier ‘vizsources’ contenant le code de l’application.

Avec la souris, faire glisser la ligne du bas de l’éditeur vers le haut, pour dé-couvrir la ligne de commande. Créer un environnement virtuel Python, en tapant :

`python -m venv .venv`

VSCode propose d’activer automatiquement cet environnement à chaque ouverture du dossier ‘vizsources’ dans VSCode : cliquer sur ‘Oui’. Activer l’environnement :

`.venv/scripts/activate`

La mention en vert <span style="color:green">(.venv)</span> doit apparaître en tête de ligne dans la ligne de commande intégrée à VSCode. On peut alors installer dans l’environnement virtuel les extensions Python nécessaires avec pip dont la liste figure dans le fichier ‘requirements.txt’ :

`pip install -r requirements.txt`

Actuellement, les paquets Python utilisés sont :

```
flask
mkdocs
mkdocs[i18n]
mkdocs-static-i18n
```

## Node.js et paquets à installer

Il faut installer la dernière version LTS de Node.js (incluant NPM) sur son PC de développement, et vérifier qu’elle est bien dans le PATH en tapant en ligne de commande :

`node  --version`  
=> affiche : v16.13.1

Configurer npm pour : le proxy de l’université, et les certificats SSL (pour éviter une erreur) :

```
npm config set proxy "http://cache.univ-poitiers.fr:3128" 
npm config set https-proxy "http://cache.univ-poitiers.fr:3128"
npm set strict-ssl false
```  
NB : delete est l'inverse de set, si on est en télétravail.

Installation en global du gestionnaire de paquets Yarn, en « old » version :

```
npm i -g yarn
yarn --version
```  
=> affiche : 1.22.17

Configurer Yarn pour le proxy :
```
yarn config set proxy http://cache.univ-poitiers.fr:3128
yarn config set strict-ssl false
```

Si le fichier [package.json](package.json) existe déjà dans le dossier (cas génaral), la liste des paquets de Node.js à installer figure dedans. Pour installer tous les paquets de la liste, il suffit de lancer la commande :

`yarn install`

Si le fichier package.json est manquant, il faut re-créer le projet et ré-installer les librairies. Voici la procédure.

- Pour créer un projet et installer (versions minimales) : React v17, Webpack v5 et Babel v7 (-D pour développement) :

  ```
  yarn init -y
  yarn add react react-dom
  yarn add -D webpack webpack-cli webpack-dev-server clean-webpack-plugin
  yarn add -D @babel/core @babel/preset-env @babel/preset-react babel-loader
  yarn add -D style-loader css-loader
  ```

  Créer le fichier de configuration de Babel pour activer ses environnements pré-définis : [babel.config.js](babel.config.js) avec ce contenu initial :

  ``` JS
  module.exports = {
    presets: [
        "@babel/preset-env", 
        "@babel/preset-react"
    ],
  };
  ```

- Pour ce projet spécifiquement, installer Leaflet et React Leaflet qui font l'affichage de cartes :

  `yarn add leaflet react-leaflet`

- Pour faire les accès à l'API porticapi qui fournit les données à afficher (http://data.portic.fr/api port 80 en production, http://127.0.0.1/api:80 en développement) :

  `yarn add axios`

- Pour la présentation, utilisation de Bootstrap 5.1 (qui dépend de popperjs), et de React-Bootstrap v2 :

  `yarn add bootstrap @popperjs/core react-bootstrap`

- Pour l'affichage d'une légende dans un coin de carte :

  `yarn add react-leaflet-custom-control`

- Pour importer les fichiers de cartes TopoJson et les convertir en GeoJson, une librairie est nécessaire :

  `yarn add topojson-client`

- Pour les calculs géométriques :

  `yarn add leaflet-geometryutil`

- Pour la localisation de l'application :

  `yarn add i18next react-i18next i18next-browser-languagedetector`

  Voir plus loin dans ce document le chapitre consacré à l'internationalisation de l'application.

- Pour les icônes FontAwesome Free en v6, avec le composant React officiel qui utilise SVG + JS :

  `yarn add @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/free-regular-svg-icons @fortawesome/free-brands-svg-icons @fortawesome/react-fontawesome@latest`

  Pour importer les icônes dynamiquement (seulement celles réellement utilisées) et ainsi alléger le bundle de l'application, il faut aussi installer un plugin de Babel :

  `yarn add babel-plugin-macros`

  et dans [babel.config.js](babel.config.js) dans `module.exports` ajouter :

  ``` JS
  plugins: [
          "macros"
      ],
  ```

  Ainsi le fichier [babel.config.js](babel.config.js) contient maintenant :

  ``` JS
  module.exports = {
    presets: [
        "@babel/preset-env", 
        "@babel/preset-react"
    ],
    plugins: [
        "macros"
    ],
  };
  ```

  puis créer un fichier [babel-plugin-macros.config.js](babel-plugin-macros.config.js) contenant :

  ``` JS
  module.exports = {
    'fontawesome-svg-core': {
      'license': 'free'
    }
  }
  ```

- Pour exporter la carte affichée dans un fichier image :

  `yarn add dom-to-image`


## Build du code frontend et lancement

Le build est assuré par Webpack. Le but est d’obtenir un fichier de code minifié : **vizsources.bundle.js**, servi par Flask à partir du répertoire **‘static/dist’** de l’application. Webpack mettra également les styles CSS dans le bundle, et leurs fichiers images ou autres nécessaires dans ce dossier (renommés avec un code de hashage pour gérer la mise en cache).

L’application [Flask (framework backend en Python)](https://flask.palletsprojects.com) effectue les tâches suivantes :
- sert d'interface avec le serveur Web (Apache 2 en production) ;
- gère les routes de l'application :
  - **'/'** : sert la page d'accueil [index.html](templates/index.html) qui se trouve dans le dossier *templates* ([templating](https://flask.palletsprojects.com/en/2.2.x/tutorial/templates/) avec [jinja](https://jinja.palletsprojects.com)). Cette page d'accueil affiche l'application React *VizSources* ;
  - **'/static/manual'** : sert le site statique d'aide utilisateur (derrière le lien *Aide*), construit avec [MkDocs](https://www.mkdocs.org/), et notamment les pages d'accueil française **/fr/** et anglaise **/en/** de l'aide. La configuration de MkDocs se trouve dans [mkdocs.yml](mkdocs.yml), et les fichiers source de la documentation, écrite en Markdown, sont dans le dossier *docs*.

L'application Flask en Python est dans le fichier : [\_\_init\_\_.py](__init__.py)

Pour afficher l’application React, le fichier HTML [templates/index.html](templates/index.html) comporte deux éléments importants :  
- une balise qui contiendra le rendu de l’application React :  
  ```html
  <div id="render-react"></div>
  ```  
  Cette balise reçoit les paramètres de l'application, dans son dataset : tous les paramètres sont préfixés par "data-". Voici un exemple avec le paramètre **'apiurl'** qui sera passé sous ce nom dans les propriétés (props) de l'application React :
  ```html
  <div id="render-react"
    data-apiurl="http://127.0.0.1/api"
  ></div>
  ```  
  
- une balise `<script></script>` qui référence le code minifié du fichier : **static/dist/vizsources.bundle.js**.  
  Dans ce bundle figurent également les styles. Les feuilles de styles CSS qui sont incluses dans le code seront "greffées" au `<head>` de la page html lors du chargement dans le navigateur.

Le code de l’application React a pour point d’entrée : [react_app/src/index.js](react_app/src/index.js), et le code source est dans le répertoire **react_app/src**. Ce fichier est un chargeur. Il ajoute à la page HTML le composant principal (racine ou root) de l'application React (dans la div #render-react), avec ses paramètres :  
`<VizSources ... />`  
Le code de ce composant React est dans le fichier [react_app/src/VizSources.jsx](react_app/src/VizSources.jsx)

La configuration de Webpack est dans [webpack.config.js](webpack.config.js), et déclare pour l’instant un seul point d’entrée nommé **vizsources** qui correspond au fichier [react_app/src/index.js](react_app/src/index.js). Pour ce point d'entrée, Webpack produit en sortie le fichier de code minifié : **static/dist/vizsources.bundle.js**, servi directement par Flask au navigateur lors du chargement de la page d'accueil.

La configuration de Webpack est inspirée de ces deux articles :  
- https://dev.to/icewreck/react-flask-integration-part-1-setup-with-webpack-djo 
- https://chiheb-nexus.github.io/flask-react-app/ 

Elle a été adaptée pour Webpack v5 et utilise : `type: 'asset/resource'` pour la gestion des images (qui sont utilisées dans les styles CSS ou déclarées dans le code JavaScript) au lieu du chargeur [file-loader](https://v4.webpack.js.org/loaders/file-loader/) qui est déprécié.

Pour lancer Webpack pendant le développement React, trois options sont prévues dans le fichier package.json, à la section **‘scripts’**. A chacun de ces scripts correspond une commande yarn à lancer :
- `yarn run dev` : pour compiler une seule fois le code React **en mode ‘développement’**. Utile au début, quand il y a des erreurs et qu'on a besoin de récupérer la main pour lancer d'autres commandes... ;
- `yarn run watch` : pour re-compiler le code **en mode ‘développement’** à chaque fois qu’un fichier est modifié / enregistré. Nécessite une ligne de commande dédiée (commande bloquante). C’est ce mode qu’on utilisera pendant tout le développement, en lançant en parallèle l’exécution de Flask ;
- `yarn run build` : à utiliser juste **avant de déployer en production**, pour compiler en mode ‘production’. En mode production (sur le serveur), Webpack n'est plus disponible (installé en mode dev uniquement).

Pour lancer le serveur Flask permettant de voir le résultat dans le navigateur :  
- Soit on crée une *'configuration de déboguage'* avec VSCode : 
  1. on clique sur l’icône du triangle : "Exécuter et déboguer" (dans la colonne sur fond noir la plus à gauche de l'interface),
  2. on clique sur le lien permettant de créer un fichier **launch.json**,
  3. dans la liste déroulante qui s’ouvre, on choisit le modèle de **configuration d’exécution : ‘Flask’**
  4. dans le squelette de code qui est automatiquement généré, on spécifie le nom du fichier python de l’application à lancer : `__init__.py`
  5. on démarre Flask en cliquant sur "Exécuter et déboguer", puis sur le triangle à gauche de la liste déroulante des configuration d’exécutions (en haut à gauche de l’écran)
  6. quand Flask a démarré, on fait Ctrl + clic sur le lien indiqué dans la console, pour lancer le navigateur Web et ouvrir la page d’accueil de l’application Web.

- >Soit on lance le serveur en exécutant le fichier [\_\_init\_\_.py](__init__.py) tout simplement (les deux lignes de code à la fin du fichier \_\_init\_\_.py le permettent).  

Et on peut utiliser en parallèle `yarn run watch` pour recompiler à la volée.  
Pour voir les modifications dans le navigateur, il faut **désactiver le cache** en cochant la case correspondante des *'outils de développement Web'* du navigateur, située dans l'onglet "Réseau" à droite des autres onglets : Inspecteur / Console / Débogueur / ...

Ainsi on peut avoir :
- **un seul site** Flask
- **plusieurs SPA** (*Single Page Application*) en REACT.

A chaque nouvelle SPA en React, correspond une page Web **nouvelle_appli_react.html** du site Flask, chargeant son propre bundle de code **nouvelle_appli_react.bundle.js**. On aura donc un nouveau point d'entrée JavaScript **nouvelle_appli_react.js**, et dans le fichier [webpack.config.js](webpack.config.js) on ajoutera une nouvelle entrée à la section 'entry' pour produire le bundle de code :
```JSON
entry: {
        vizsources: "./react_app/src/index.js",
        nouvelle_appli_react: "./react_app/src/nouvelle_appli_react.js",
    },
```

Il serait alors judicieux d'avoir les ressources partagées par les deux pages dans un bundle commun (voir les options d'optimisation de Webpack, et plus particulièrement [optimization.splitChunks](https://webpack.js.org/configuration/optimization/#optimizationsplitchunks)). A ce moment-là, il faudra installer le plugin [SplitChunksPlugin](https://webpack.js.org/plugins/split-chunks-plugin/) de Webpack, et le configurer.


# Internationalisation

L'internationalisation de l'application consiste à afficher l'application dans deux langues : l'Anglais (par défaut), et le Français.

Cette internationalisation a lieu à 4 niveaux :

- dans **le code de l'application** - les paquets Node.js suivants sont utilisés :
  - *i18next* : 
    - chargement des fichiers de traduction : un fichier .json par langue gérée (ex.: [en.json](jsx/translate/../../static/jsx/translate/locales/en.json) ), comportant des associations clé / valeur, regroupées sous des préfixes permettant d'identifier la zone (fichier) de l'application dans laquelle la traduction se situe. 
    
      Exemple :
      ``` JSON
      {
        "vizSources": {
          "load_in_progress" : "Loading data ...",
          "load_error": "Error while loading data : "
        },
        "worldOverlay": {
          "overlay_name_default": "World in 1789"
        }
      }
      ```
    - chargement des plugins (dont la détection de langue du navigateur), 
    - enregistrement de la préférence de langue de l'utilisateur dans le stockage local du navigateur, lorsque l'utilisateur sélectionne une langue différente de celle par défaut sur son poste,
    - langue par défaut (l'Anglais), de repli, si la langue du navigateur n'est pas dans la liste des langues gérées (en, fr),
    - d'autres fonctions plus avancées : formatage des dates, gestion des pluriels, interpolation (intégrer des valeurs de variables dans les traductions - par exemple `{{count}}`)
    
    En pratique, ce paquet fournit deux fonctionnalités importantes :
    - obtenir la langue en cours : `i18n.resolvedLanguage`
    - changer la langue (en sélectionner une autre) : `i18n.changeLanguage('en')`
  - *react-i18next* : l'adaptation à React de la librairie i18next. Elle fournit deux moyens de traduire les chaines présentes dans le code de l'application :
    - la fonction de traduction : t()
      ``` JS
      t('prefix.clé_de_traduction', 'valeur de traduction (par défaut si échec dans la recherche des traductions)')
      ```
      Le second paramètre de la fonction améliore grandement la lisibilité du code source
    - un composant : Trans, qui conserve les tags ou éléments React figurant dans la traduction, ce qui évite le saucissonnage des chaînes complexes :
      ``` HTML
      <Trans i18nKey="prefix.clé_de_traduction">valeur de <strong>traduction</strong> par défaut</Trans>
      ```
    De plus, si un composant React a utilisé le **hook useTranslation**, lorsqu'un changement de langue survient, le composant est automatiquement ré-affiché (rafraîchi), donc le changement de langue s'applique immédiatement à l'écran
  - *i18next-browser-languagedetector* : c'est un plugin de i18next qui détecte la langue par défaut du navigateur du visiteur. La langue est automatiquement chargée et affichée.  
  En complément, il faut fournir un **sélecteur de langue** dans l'application, pour que l'utilisateur puisse choisir une langue différente. Dans l'application, c'est le composant : [LangSelector.jsx](static/jsx/translate/LangSelector.jsx)
- dans **la documentation** - un paquet Python d'internationalisation du **thème** de MkDocs, *mkdocs[i18n]* (`pip install mkdocs[i18n]`), et un plugin d'internationalisation du **contenu** de la documentation, [mkdocs-static-i18n](https://pypi.org/project/mkdocs-static-i18n/), sont utilisés dans le générateur de documentation [MkDocs](https://www.mkdocs.org/), et la documentation est rédigée en Markdown dans les deux langues (en, fr). Le plugin utilisé permet :
  - d'avoir des fichiers traduits avec un suffixe .lang.md (ex.: index.fr.md, index.en.md), mais en gardant des liens avec le suffixe .md qui dirigeront vers la page de la langue actuellement sélectionnée;
  - d'avoir aussi des assets traduits : par exemple des images image.fr.jpg et image.en.jpg, avec des liens vers image.jpg qui afficheront l'image de la langue en cours.
   
  Pour construire la documentation, après avoir activé son environnement virtuel Python, et s'être assuré d'avoir installé les modules nécessaires (mkdocs, mkdocs[i18n] et mkdocs-static-i18n), lancer :  
  `mkdocs build`

  Pour rajouter un sélecteur de langue, il n'existe pas de solution toute faite, mais dans la documentation du thème "mkdocs" on trouve [comment personnaliser ce thème](https://www.mkdocs.org/user-guide/customizing-your-theme/#using-the-theme-custom_dir), et l'auteur du plugin d'internationalisation des textes [donne des pistes](https://pypi.org/project/mkdocs-static-i18n/#writing-a-custom-language-switcher) : dans le dossier [docs_theme_overrides](docs_theme_overrides/) on surcharge le template Jinja [base.html](docs_theme_overrides/base.html) en :

  - neutralisant le lien vers la page d'accueil situé autour du titre :
    ``` jinja
    {%- block site_name %}
      <a class="navbar-brand" href="#">{{ config.site_name }}</a>
    {%- endblock %}
    ```
  - insérant le sélecteur de langue, juste avant le moteur de recherche :
    ``` jinja
    {% if "i18n" in config['plugins'] %}
      {% include "i18n_languages.html" %}
    {% endif %}
    ```

  Le code du sélecteur de langue est dans le template Jinja [i18n_languages.html](docs_theme_overrides/i18n_languages.html). Il prend la forme d'une liste déroulante proposant les langues disponibles (noms à 2 lettres) : 
  ``` jinja
  <li class="nav-item" style="display: flex;align-items: center;">
    <select size="1" onChange="location = this.options[this.selectedIndex].value;">
        {% for opt_lang, opt_display in config.plugins.i18n.config.languages.items() -%}
            <option class="nav-link" value="../{{ opt_lang }}/index.html" {% if opt_lang == i18n_page_locale %}selected{% endif %}>{{ opt_lang }}</option>
        {% endfor %}
    </select>
  </li>
  ```
  Attention : le plugin redirige systématiquement vers la page d'accueil au moyen d'une URL relative, et ne fonctionne pas si on accède à la page d'accueil par défaut (sans /en/ ou /fr/ dans son URL) ! C'est la raison pour laquelle on a neutralisé dans base.html le lien vers la page d'accueil, qui est spécifié comme un lien vers la version par défaut du site (version dans laquelle le sélecteur de langue est inefficace).

- dans **l'API Porticapi** - le paramètre **lang=fr** est par exemple passé dans chaque requête. Ceci implique de recharger les données de l'API lorsque l'utilisateur choisit une langue différente
- dans **le fond de carte** - les noms des régions du monde en 1789 (shortname) figuraient initialement en français seulement dans le fichier de données au format TopoJSON. Une version anglaise (shortname_en) a été ajoutée dans le fichier.



