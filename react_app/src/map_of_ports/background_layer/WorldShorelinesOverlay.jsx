// WorldShorelinesOverlay.jsx
/**
 * Couche fond de carte avec les côtes seulement (pas les régions : régions à charger par la suite, selon l'année, sous forme d'une autre couche)
 * Attention problème de volume de données (trop) important, qui fait planter l'export jpg/png/svg si on ne simplifie pas avec Mapshaper
 * Sources possibles pour les données :
 * - Natural Earth au 1:10m : http://www.naturalearthdata.com/downloads/10m-physical-vectors/10m-coastline/
 * faire glisser tous les fichiers dézippés (format shapefile) dans Mapshaper pour simplification (garder les îles proches France) et export TopoJSON
 * (Pb.: les continents ne sont pas fermés, surtout aux longitudes extrêmes +180° et -180°, ils apparaissent dans Leaflet avec une diagonale !)
 * - Données de la NOAA (NASA), open, gshhs i(ntermediate) couche L1 (format shapefile) : https://www.ngdc.noaa.gov/mgg/shorelines/gshhs.html lien 'download'
 * polygones fermés OK. Simplification nécessaire aussi (mapshaper) avant export TopoJSON
 * => adopté temporairement initialement pour maquetter (simplification 15,2% trop forte d'où absence des îles françaises, 
 * mais indispensable pour ne pas planter l'export jpg, en attendant mieux)
 * - Couche des traits de côtes dans base Portic, simplif. nécessaire dans QGIS avant export de la couche en shapefile, car sinon trop voluminaux pour Mapshaper
 * 
 * Rend un composant <LayersControl.Overlay> 
 * => à insérer dans un <LayersControl> (lui-même enfant du composant React-Leaflet : <MapContainer>)
 * Propriétés à passer :
 * - checked (defaut : false - pour faire initialement apparaître la couche)
 * - name (defaut : "Monde en 1789" - nom de la couche qui apparaîtra dans le <LayersControl>)
 * - lookOptions (un style par défaut est fourni - style appliqué aux régions GeoJSON, voir Path options : https://leafletjs.com/reference.html#path-option )
 * 
 * translate prefix : worldOverlay
 */

import React from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { LayersControl, GeoJSON, LayerGroup } from 'react-leaflet'
import { TopoJSON } from './TopoJSON';
import { useTranslation } from 'react-i18next';
// import worldShorelines from '../../../maps/trait_cote_monde_gshhs_i_L1_4326_minified2dec.topojson'
// import worldShorelines from '../../../maps/ne_10m_coastline.topojson'
// import worldShorelines from '../../../maps/cotes_gshhs_mapshaper_simplifie_V_weighted_15,2p.topojson'
import worldShorelines from '../../../maps/ne_10m_land'


export const WorldShorelinesOverlay = ({checked, name, lookOptions}) => {
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    // Pour TopoJson : map.topojson.js
    const addTooltipWithNameOfRegion = (feature, layer) => {
        // console.log(feature)
    }


    // Rendu du composant :

    // Si l'overlay est visible (coché) initialement :
    const isChecked=(checked == undefined) ? false : checked;

    // Nom de l'overlay : si 'name' pas spécifié, mettre une valeur par défaut :
    const overlayName=(name == undefined) ? t('worldOverlay.overlay_name_shorelines', "World (shorelines only)") : name;

    // Style pour la carte du monde avec seulement les rivages (côtes) :
    // Styling of GeoJSON : style, with Path options : https://leafletjs.com/reference.html#path-option
    const worldMapLookDefault={
        // borders on polygons :
        stroke: true,
        color: "#000000",
        weight: 0.75,
        opacity: 0.5,
        // filling of polygons :
        fill: true,
        fillColor: "#ddb857",
        fillOpacity: 0.5,
        background: "#ffffff",
        // Custom CSS class name set on an element. Only for SVG renderer.
        // className: xxx;
    }
    // et : couleur de fond de la carte dans un style CSS à définir - voir world-background.css :
    /* Couleur de fond de la carte en GeoJSON 
    .leaflet-container {
	    background: #e7f6fc;
	    outline: 0;
	}
    */

    // Si pas de mise en forme spécifiée, reprendre le look par défaut 
    const worldMapLook=(lookOptions == undefined) ? worldMapLookDefault : lookOptions;

    return (
        <LayersControl.BaseLayer checked={isChecked} name={overlayName} > 
            <LayerGroup>

                <TopoJSON
                    data={worldShorelines} 
                    style={worldMapLook} 
                    onEachFeature={addTooltipWithNameOfRegion} 
                    pane={'tilePane'} 
                />
                
            </LayerGroup>
        </LayersControl.BaseLayer>
    )
}

