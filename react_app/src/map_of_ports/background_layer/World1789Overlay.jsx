// World1789Overlay.jsx
/**
 * Couche fond de carte du monde en 1789
 * Rend un composant <LayersControl.Overlay> 
 * => à insérer dans un <LayersControl> (lui-même enfant du composant React-Leaflet : <MapContainer>)
 * Propriétés à passer :
 * - checked (defaut : false - pour faire initialement apparaître la couche)
 * - name (defaut : "Monde en 1789" - nom de la couche qui apparaîtra dans le <LayersControl>)
 * - lookOptions (un style par défaut est fourni - style appliqué aux régions GeoJSON, voir Path options : https://leafletjs.com/reference.html#path-option )
 * 
 * translate prefix : worldOverlay
 */

import React, { useState, useEffect } from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { LayersControl, GeoJSON, LayerGroup, Marker, Tooltip } from 'react-leaflet'
import { useTranslation } from 'react-i18next';
import { world1789 } from '../../../maps/cartoweb_world_1789_02fevrier2022_mixte4326.toposon'
import { TopoJSON } from './TopoJSON';
import { RegionLabel } from '../datamodel/RegionLabel';

export const World1789Overlay = ({checked, name, lookOptions}) => {
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    // State :
    const [listTooltipsAAjouter]=useState(new Array());
    const [geojsonElementCreated, setGeojsonElementCreated] = useState(false);
    const [listTooltipsAAjouterComplete, setListTooltipsAAjouterComplete] = useState(false);

    // Ajoute les noms des régions à créer dans la liste 'listTooltipsAAjouter', lors du chargement de la carte GeoJSON du monde en 1789
    // Cette fonction d'événement est appelée pour chaque "feature" du fichier GeoJSON, après sa transformation en (multi)polygone "layer"
    /*
    // Pour GeoJson :
    const addTooltipWithNameOfRegion = (feature, layer) => {
        // Créer un objet avec les infos nécessaires à la création / positionnement / tri des étiquettes (tooltip) des régions :
        const id=feature.properties.id;
        const nomRegion=feature.properties.shortname;   // Texte de l'étiquette
        const latitude=feature.properties.lat;  // Coordonnées pour placer l'étiquette
        const longitude=feature.properties.long;
        const pointPlacementEtiquette=L.latLng({ lat: latitude, lng: longitude});   // coordonnée spatiale Leaflet de l'étiquette
        const rectangleEnglobant=layer.getBounds(); // classe : LatLngBounds, rectangle englobant la forme de la région (pour le calcul d'aire=taille de la région)

        const tooltipAAjouter=new RegionLabel(id, nomRegion, pointPlacementEtiquette, rectangleEnglobant);    // Instanciation classe RegionLabel

        // Ajout de l'objet à la liste des étiquettes à créer :
        listTooltipsAAjouter.push(tooltipAAjouter);

        // Ajout direct d'une étiquette (Tooltip) avec le nom de chaque région :
        // (On ne garde pas les coordonnées du centre de 'layer' : bug Leaflet avec les multipolygones)
        // => layer.bindTooltip() est inutilisable ici
        // Voir plutôt : https://stackoverflow.com/questions/49946323/how-to-define-an-additional-tooltip-in-leaflet
        
        // Fonction factory de Leaflet pour créer un Tootlip :
        //L.tooltip({
        //    permanent: true,
        //    opacity: 0.5,
        //    direction: "center", 
        //    className: "region-name-tooltip"
        //}, layer)
        //    .setContent(nomRegion)
        //    .setLatLng({ lat: latitude, lng: longitude})    // permet de choisir les coordonnées à utiliser
        //    .addTo(map);  
        
    }
    */
    /*
    // Pour TopoJson : countries-110m.json.js
    const addTooltipWithNameOfRegion = (feature, layer) => {
        // Créer un objet avec les infos nécessaires à la création / positionnement / tri des étiquettes (tooltip) des régions :
        var validObject=true;

        const id=feature.id;
        const nomRegion=feature.properties.name;   // Texte de l'étiquette
        if((id == undefined) || (nomRegion == undefined)) {
            validObject=false;
            console.log("-----")
            console.log("invalid feature :")
            console.log("id: "+id)
            console.log("name: "+nomRegion)
            console.log(feature);
        }
        // const latitude=feature.properties.lat;  // Coordonnées pour placer l'étiquette
        // const longitude=feature.properties.long;
        // const pointPlacementEtiquette=L.latLng({ lat: latitude, lng: longitude});   // coordonnée spatiale Leaflet de l'étiquette
        const bounds=layer.getBounds(); // classe : LatLngBounds, rectangle englobant la forme de la région (pour le calcul d'aire=taille de la région)
        const pointPlacementEtiquette=bounds.getCenter();   // coordonnée spatiale Leaflet de l'étiquette

        if(validObject) {
            const tooltipAAjouter=new RegionLabel(id, nomRegion, pointPlacementEtiquette, bounds);    // Instanciation classe RegionLabel
            // Ajout de l'objet à la liste des étiquettes à créer :
            listTooltipsAAjouter.push(tooltipAAjouter);
        }

        // Reste le problème des polygones qui "wrappent" :
        // https://stackoverflow.com/questions/40532496/wrapping-lines-polygons-across-the-antimeridian-in-leaflet-js 
        
    }
    */
    // Pour TopoJson : map.topojson.js
    const addTooltipWithNameOfRegion = (feature, layer) => {
        // Créer un objet avec les infos nécessaires à la création / positionnement / tri des étiquettes (tooltip) des régions :
        const id=feature.properties.id;
        const nomRegion=(lang == 'fr' ? feature.properties.shortname : feature.properties.shortname_en);   // Texte de l'étiquette
        const latitude=feature.properties.lat;  // Coordonnées pour placer l'étiquette
        const longitude=feature.properties.long;
        const pointPlacementEtiquette=L.latLng({ lat: latitude, lng: longitude});   // coordonnée spatiale Leaflet de l'étiquette
        const rectangleEnglobant=layer.getBounds(); // classe : LatLngBounds, rectangle englobant la forme de la région (pour le calcul d'aire=taille de la région)

        const tooltipAAjouter=new RegionLabel(id, nomRegion, pointPlacementEtiquette, rectangleEnglobant);    // Instanciation classe RegionLabel

        // Ajout de l'objet à la liste des étiquettes à créer :
        listTooltipsAAjouter.push(tooltipAAjouter);
    }


    // Attendre que l'élément <GeoJSON> soit chargé pour exploiter la liste des tooltips à créer
    useEffect(() => {
        if(geojsonElementCreated) { // Ne pas faire initialement, seulement quand la variable passe à true
            // La liste des étiquettes à créer est complète, 
            // on doit la trier par ordre décroissant des tailles de régions, 
            // pour gérer la collision des étiquettes lors du zoom, en ajoutant d'abord à la carte les plus grandes régions :
            listTooltipsAAjouter.sort(RegionLabel.fonctionComparaison);
            // Autoriser maintenant la création de la liste des tooltips dans le rendu :
            setListTooltipsAAjouterComplete(true);  
        }
    }, [geojsonElementCreated]);

    /*
    const dumpList = () => {
        console.log(listTooltipsAAjouter);
        console.log(listTooltipsAAjouter.length);
        listTooltipsAAjouter.forEach(tooltipAAjouter => {
            console.log(tooltipAAjouter);
        });
    };
    */


    // Rendu du composant :

    // Si l'overlay est visible (coché) initialement :
    const isChecked=(checked == undefined) ? false : checked;

    // Nom de l'overlay : si 'name' pas spécifié, mettre une valeur par défaut :
    const overlayName=(name == undefined) ? t('worldOverlay.overlay_name_world1789', "World in 1789") : name;

    // Style pour la carte du monde en 1789 :
    // Styling of GeoJSON : style, with Path options : https://leafletjs.com/reference.html#path-option
    const worldMapLookDefault={
        // borders on polygons :
        stroke: true,
        color: "#000000",
        weight: 0.75,
        opacity: 0.5,
        // filling of polygons :
        fill: true,
        fillColor: "#ddb857",
        fillOpacity: 0.5,
        background: "#ffffff",
        // Custom CSS class name set on an element. Only for SVG renderer.
        // className: xxx;
    }
    // et : couleur de fond de la carte dans un style CSS à définir - voir world-background.css :
    /* Couleur de fond de la carte en GeoJSON 
    .leaflet-container {
	    background: #e7f6fc;
	    outline: 0;
	}
    */

    // Si pas de mise en forme spécifiée, reprendre le look par défaut 
    const worldMapLook=(lookOptions == undefined) ? worldMapLookDefault : lookOptions;

    return (
        <LayersControl.BaseLayer checked={isChecked} name={overlayName} > 
            <LayerGroup>

                <TopoJSON
                    data={world1789} 
                    style={worldMapLook} 
                    onEachFeature={addTooltipWithNameOfRegion} 
                    pane={'tilePane'} 
                />
                {!geojsonElementCreated && setGeojsonElementCreated(true) }

                <LayerGroup>
                    { listTooltipsAAjouterComplete && listTooltipsAAjouter.map(region => (
                        <Marker position={region.point} icon={L.divIcon({className: "nonExistingCSSClassToHideMarker"})} key={region.id} >
                            <Tooltip 
                                permanent
                                opacity={0.5} 
                                direction={"center"} 
                                className={"region-name-tooltip"}>{region.name}</Tooltip>
                        </Marker>
                    )) 
                    }
                </LayerGroup>
                
            </LayerGroup>
        </LayersControl.BaseLayer>
    )
}
