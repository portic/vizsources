// MapOfFrenchPorts.jsx
/**
 * Carte des ports de France au XVIIIème siècle (1787-1789) :
 * - Fond de carte vectoriel (World1789Overlay) : régions du monde, avec étiquettes de noms
 *      -> modif. d'octobre 2022 : on met en fond, la carte avec seulement les rivages (traits de côtes)
 *      -> une couche avec les limites administratives correspondant à l'année choisie sera superposée. Elle sera décochable.
 * - Couche des congés (PortsCongesOverlay) : étiquette=nom, Cercle bleu d'aire proportionnelle au nombre de congés, point noir si pas de donnée
 *   + symbole indiquant la Présence d'un greffier dans un port (PortHasAClerk) : carré violet
 * - Couche des variables de complétion (PortsVariableFilledOverlay) : étiquette=nom, cercle d'aire proportionnelle au total des pointcalls du port, couleur selon pourcentage de complétion
 * Rq.: le composant PortNames d'affichage des noms des ports, est intégré aux couches : congés, et variables (une seule affichée à la fois)
 * - Couche des limites d'amirautés, affichée avec les autres couches, décochable
 * - Légende : regroupe les éléments de légende de chaque couche. Refermable pour faciliter l'utilisation sur smartphone.
 * - Gestion collision des étiquettes par groupes : ajout étiquettes dans l'ordre décroissant du nb. de congés (ports), ou de la taille (régions)
 * 
 * React-Leaflet : https://react-leaflet.js.org/
 * Leaflet : https://leafletjs.com/
 * 
 * translate prefix : portsMap
 */

import React, { useState, useEffect } from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { MapContainer, LayersControl } from 'react-leaflet'
import { useTranslation } from 'react-i18next';
import { WorldShorelinesOverlay } from './background_layer/WorldShorelinesOverlay'
import { World1789Overlay } from './background_layer/World1789Overlay'
import { AdmiraltyLimitsOverlay } from './background_layer/AdmiraltyLimitsOverlay'
import '../../css/world-background.css'
import { PortConges } from './ports_layers/PortConges'
import { PortVarCompleteness } from './ports_layers/PortVarCompleteness'
import { DetectOverlappingTooltips } from './DetectOverlappingTooltips'
import { Legend } from './legend/Legend'
import { UpdateMapParameters } from './UpdateMapParameters'
// Classes de données :
import { PortData } from './datamodel/PortData'
import { Layer } from './datamodel/Layer'
import { ListLayers } from './datamodel/ListLayers'

// Composants HOC :
import { composePortsOverlay } from './ports_layers/ComposePortsOverlay'
const PortsCongesOverlay=composePortsOverlay(PortConges); // couche de carte : congés des ports
const PortsVariableFilledOverlay=composePortsOverlay(PortVarCompleteness);    // pourcentage de pointcalls ayant la 'variable' renseignée


export const MapOfFrenchPorts = ({portsDataFromAPI, variable, source, annee, print, center, changeCenter, zoom, changeZoom, world, changeWorld}) => {
    const { t } = useTranslation();

    // Renvoie le nom à afficher pour la couche 'variable'
    const getVariableLayerName = (variable) => {
        var result="";
        var variableName="";

        // Chercher le nom de la variable dans les traductions de préfixe 'controlsViz' :
        switch(variable) {
            case 'tonnage' :
                variableName=t('controlsViz.var_tonnage', "Ship burthen");
                break;
            case 'homeport' :
                variableName=t('controlsViz.var_homeport', "Ship's homeport");
                break;
            case 'products' :
                variableName=t('controlsViz.var_products', "Cargo or scope of voyage");
                break;
            case 'birthplace' :
                variableName=t('controlsViz.var_birthplace', "Captain local identification");
                break;
            case 'citizenship' :
                variableName=t('controlsViz.var_citizenship', "Captain \"national\" identification");
                break;
        }
        // Composer le nom de la couche: rajouter " (complétude)"
        result=t('portsMap.variable_filled_overlay_name', "{{ variableName }} completeness", {variableName: variableName});
        return result;
    }
    
    // Dans le State :
    // * allLayersReady : quand toutes les couches sont affichées
    const [allLayersReady, setAllLayersReady] = useState(false);
    // * beginDetectOverlappingTooltips : pour (commencer à) faire la détection de collision (initiale) des tooltips
    const [beginDetectOverlappingTooltips, setBeginDetectOverlappingTooltips] = useState(false);
    // * displayVariable : variable de l'état, miroir de 'variable' mais différée, pour garantir une mise à jour correcte des couches lors de son changement
    const [displayVariable, setDisplayVariable] = useState(variable);
    // * listLayers : liste des données des couches de la carte
    const [listLayers, setListLayers]=useState(new ListLayers([
        new Layer(Layer.L_CONGES, t('portsMap.conges_overlay_name', "Number of departures or entrances"), true, true),   // données pour instancier la couche : {id, name, visible, isPortsLayer}
        new Layer(Layer.L_VARIABLE, getVariableLayerName(variable), false, true),
        new Layer(Layer.L_LIM_AMIRAUTES, t('portsMap.admiralty_lim_overlay_name', "Admiralty limits"), true, false), 
    ]));

    // console.log("allLayersReady : "+allLayersReady);
    // console.log("beginDetectOverlappingTooltips : "+beginDetectOverlappingTooltips);
    // console.log("variable : "+variable+" displayVariable : "+displayVariable);

    // Lorsque 'variable' change, on ne garde visible que la (ou les) couche(s) correspondant à la nouvelle valeur
    const updateLayersVisibilities = (variable, listLayers) => {
        // Par défaut, rendre toutes les couches invisibles :
        listLayers.setAllLayersInvisible([Layer.L_LIM_AMIRAUTES]); // Sauf les couches de la liste passée, toujours visibles (ou du moins, à ne pas modifier !)
        // Ne rendre visible que la (les) couche(s) correspondant à la nouvelle valeur de 'variable' :
        switch(variable) {
            case "conges" : // congés
                const lConges=listLayers.getLayerById(Layer.L_CONGES);
                lConges.visible=true;
                break;
            default :   // variable
                const lVariable=listLayers.getLayerById(Layer.L_VARIABLE);
                lVariable.visible=true;
                lVariable.name=getVariableLayerName(variable);  // mettre aussi à jour le nom de la couche (qui dépend de la variable)
                break;            
        }
    }

    // Faire quand tout est chargé : détection collision des étiquettes
    useEffect(() => {
        if(allLayersReady) {    // Ne pas faire initialement, seulement quand la variable passe à true
            setBeginDetectOverlappingTooltips(true);
        }
    }, [allLayersReady])

    // Faire sur changement de 'variable' : 
    useEffect(() =>  {
        // MAJ visibilité des couches :
        updateLayersVisibilities(variable, listLayers);
        // Nouveau cycle pour collision étiquettes :
        if(allLayersReady) {
            setAllLayersReady(false);
        }
        if(beginDetectOverlappingTooltips) {
            setBeginDetectOverlappingTooltips(false);
        }
        // MAJ de displayVariable, pour mettre à jour la couche à afficher :
        setDisplayVariable(variable);
    }, [variable])

    
    // Rendu du composant :
    // Données des couches (datamodel) :
    const lConges=listLayers.getLayerById(Layer.L_CONGES);
    const lLimAmirautes=listLayers.getLayerById(Layer.L_LIM_AMIRAUTES);
    const lVariable=listLayers.getLayerById(Layer.L_VARIABLE);

    // Afficher les données sur une carte avec React-Leaflet :
    return (
        <MapContainer 
            style={{ height : "78vh" }} 
            center={center}
            zoom={zoom}
            scrollWheelZoom={true} 
            attributionControl={!print} 
            zoomControl={!print} 
            whenCreated={(map) => {}}
        >
            <LayersControl position="topright">
                
                <WorldShorelinesOverlay checked={world == "shorelines"} />
                <World1789Overlay checked={world == "1789"} />

                <AdmiraltyLimitsOverlay layerData={lLimAmirautes} />

                { (displayVariable == "conges") && 
                    <PortsCongesOverlay layerData={lConges} 
                        portsData={portsDataFromAPI} 
                        variable={displayVariable} source={source} annee={annee} 
                        whenReady={() => {!allLayersReady && setAllLayersReady(true)}} 
                    />
                }

                { (displayVariable != "conges") && 
                    <PortsVariableFilledOverlay layerData={lVariable} 
                        portsData={portsDataFromAPI} 
                        variable={displayVariable} source={source} annee={annee} 
                        whenReady={() => {!allLayersReady && setAllLayersReady(true)}} 
                    />
                }

            </LayersControl>

            { beginDetectOverlappingTooltips && 
                <DetectOverlappingTooltips tooltipClassesToDeClutter={"region-name-tooltip "+displayVariable} />
            }

            <Legend layers={listLayers} print={print} variable={displayVariable} source={source} annee={annee} />

            <UpdateMapParameters changeCenter={changeCenter} changeZoom={changeZoom} changeWorld={changeWorld} />

        </MapContainer>
    )

}