// Legend.jsx
/**
 * Affichage d'une légende dans un coin de la carte
 * Utilise le package yarn / composant Control, compatible React-Leaflet v3 : https://www.npmjs.com/package/react-leaflet-custom-control
 * 
 * translate prefix : legend
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, DropdownButton, Button } from 'react-bootstrap';
import React, { useState } from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { useMapEvents } from 'react-leaflet'
import Control from 'react-leaflet-custom-control'
import { Trans, useTranslation } from 'react-i18next';
import { LegendConges } from './LegendConges'
import { LegendVar } from './LegendVar'
import { LegendAdmiraltyLim } from './LegendAdmiraltyLim'
import { Layer } from '../datamodel/Layer'
import { ListLayers } from '../datamodel/ListLayers'


export const Legend = ({layers, print, variable}) => {
    const { t } = useTranslation();

    // State :
    const [displayUpdateNumber, setDisplayUpdateNumber]=useState(0);    // Numéro de mise à jour (pour la forcer)
    const [closed, setClosed]=useState(false);  // Ouvrir / fermer le bloc de légende

    // Mise en forme du titre "Légende"
    const textNoHorizontalMargin="px-0";

    // Pour la mise en forme (Bootstrap) identique de toutes les légendes des couches
    const legendClassnames={
        textNoHorizontalMargin: textNoHorizontalMargin,
        textFromLeft: "pe-0",
        layerNameRow: "pb-1 pt-2",
        legendItemRow: "align-items-center pb-1",
        symbolColWidthInGrid: 3,
        symbolCol: "ps-0 pe-1",
        textColWidthInGrid: "auto"
    }
    
    // Tableau de correspondance : id de couche -> composant de légende à afficher
    const layerConges=layers.getLayerById(Layer.L_CONGES);
    const layerLimAmirautes=layers.getLayerById(Layer.L_LIM_AMIRAUTES);
    const layerVarCompleteness=layers.getLayerById(Layer.L_VARIABLE);
    const tabLegendComponents=[
        <LegendConges layer={layerConges} legendClassnames={legendClassnames} key={Layer.L_CONGES} />,
        <LegendVar layer={layerVarCompleteness} legendClassnames={legendClassnames} key={Layer.L_VARIABLE} />,
        <LegendAdmiraltyLim layer={layerLimAmirautes} legendClassnames={legendClassnames} key={Layer.L_LIM_AMIRAUTES} />
    ];

    // Renvoie un tableau contenant les légendes des couches visibles
    const getLegendsToDisplay = (listLayers) => {
        var result=[];

        // Parcourir la liste des couches visibles :
        const visibleLayers=listLayers.getVisibleLayers();    // obtenir la liste
        result=visibleLayers.map((layer) => {  // pour chaque couche visible
            layer.computeDataForLegend();   // MAJ des données pour formatter le contenu
            return tabLegendComponents[layer.id];   // Légende de la couche
        })
        return result;  // Tableau de légendes à afficher
    }

    // Met à jour l'attribut 'visible' de la couche mentionnée dans l'événement
    const updateLayerVisibility = (event, newVisibility) => {
        // retrouver la couche cochée ou décochée, en la cherchant par son nom :
        const layerName=event.name; // nom de la couche cochée / décochée à retrouver, puis mettre à jour (visibilité)
        var layer=null  // couche concernée à retrouver d'abord

        // Retrouver la couche correspondant au nom de la couche cochée / décochée :
        // (difficulté : la couche des ports est identifiée "Noms de ports", et non plus par son nom réel (congés, tonnage, ...) qui permettait de la retrouver directement)
        if(layerName == t('portsMap.layer_name_ports', "Port names")) { // Si la couche cochée / décochée s'appelle "Noms de ports"
            // Rechercher la couche ayant les noms des ports :
            layer=layers.getPortsLayerByVariable(variable)  // Cherche la couche avec les noms des ports, en fonction de la variable affichée (congés ou var. de complétude)
        } else {    // Sinon, retrouver la couche directement par son nom d'affichage
            layer=layers.getLayerByName(layerName)
        }

        if(layer != undefined) {    // Si la couche est dans la liste
            // Changer la visibilité de la couche
            layer.visible=newVisibility;
            // Mettre à jour l'affichage du composant :
            setDisplayUpdateNumber(displayUpdateNumber+1);
        }
    }

    // Pour mettre à jour le contenu de la légende, quand des couches sont cochées / décochées : enregistrer des gestionnaires d'événements
    // ( liste des événements : https://leafletjs.com/reference.html#map-event )
    const map = useMapEvents({
        overlayadd: (event) => {
            // console.log("overlay_add");
            updateLayerVisibility(event, true); // afficher la légende de la couche
        },  // On vient de (re-)cocher une couche dans le LayersControl
        overlayremove: (event) => {
            // console.log("overlay_remove");
            updateLayerVisibility(event, false);    // cacher la légende de la couche
        },  // On vient de décocher une couche dans le LayersControl, pour ne plus l'afficher
    });

    // Rq.: le contenu de la légende est également mis à jour sur modification des props : source, annee.

    // Déplier / replier le bloc de légende
    const handleClick=() => {
        setClosed(!closed);
    }


    // Rendu du composant :
    const legendsToDisplay=getLegendsToDisplay(layers); // Contenu à afficher : les légendes des couches visibles

    // * Contenu si repliée (closed == true) : Bouton pour ré-ouvrir
    const ifClosed=<Control append position='topright' >
        <div className={"leaflet-bar"}>
            <DropdownButton variant="light" size="sm" id="legend-dropdown-button" 
                onClick={handleClick} 
                title={t('legend.legend_title', "Legend")} 
            ></DropdownButton>
        </div>
    </Control>
    
    // * Contenu si dépliée : Retourner la légende
    const ifNotClosed=<Control append position='topright' >
        <Container fluid className={"bg-white bg-opacity-75 leaflet-bar px-4"} >
            <Row className={"align-items-center pt-1"} >
                <Col className={textNoHorizontalMargin} >
                    <div className={"text-center"} >
                        <strong>
                            <Trans i18nKey="legend.legend_title">Legend</Trans>
                        </strong>
                    </div>
                </Col>
                <Col xs={"auto"} >
                    <Button 
                        variant={"outline-"+(print ? "light" : "dark")} 
                        size="sm" 
                        className={"border-0 p-0"} 
                        onClick={handleClick}
                        title={t('legend.legend_cross_hover', "fold")} 
                    >&#x1F5D9;</Button>
                </Col>
            </Row>
            { legendsToDisplay }
        </Container>
    </Control>
    
    // rien (null) si aucune couche visible :
    return ((legendsToDisplay.length == 0) ? null :  (closed ? (print ? null : ifClosed) : ifNotClosed));
}