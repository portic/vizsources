// LegendConges.jsx
/**
 * Affiche la légende de la couche : Congés (Ports avec leurs congés)
 * 
 * translate prefix : legend
 */
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'react-bootstrap';
import React from 'react';
import { LegendHasAClerk } from './LegendHasAClerk'
import { Trans, useTranslation } from 'react-i18next';
import '../../../css/legend-symbols.css'

export const LegendConges = ({layer, legendClassnames: cNames }) => {
    const { t } = useTranslation();

    // Rendu du composant
    return (<>
        <Row className={cNames.layerNameRow} >
            <Col xs={"auto"} className={cNames.textNoHorizontalMargin} >
                <u>{layer.name} : </u>
            </Col>
            <Col></Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendCircleNoConge"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                <Trans i18nKey="legend.no_data">no data</Trans>
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendCircleConges"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                <Trans i18nKey="legend.leave_fee_inputdone">input done</Trans>
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendCircleCongesCr"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                <Trans i18nKey="legend.leave_fee_reports">report data only</Trans>
            </Col>
        </Row>
        <LegendHasAClerk legendClassnames={cNames} />
        <Row className={cNames.legendItemRow} >
            <Col className={cNames.textFromLeft} >
                <Trans i18nKey="legend.leave_fee_minimum">minimum : 3</Trans>
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col className={cNames.textFromLeft} >
                <Trans i18nKey="legend.leave_fee_mean">average : </Trans>{layer.moyenneConges}
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col className={cNames.textFromLeft} >
                <Trans i18nKey="legend.leave_fee_maximum">maximum : </Trans>{layer.maxConges}
            </Col>
        </Row>
    </>)
}