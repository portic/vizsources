// Layer.js
/**
 * Données sur une couche de la carte
 */

import { PortData } from './PortData'

export class Layer {
    // Membres de l'instance :
    id=0;
    name="";
    visible=false;
    isPortsLayer=false; // Indique si c'est la couche contenant les noms des ports (pour pouvoir la retrouver si l'utilisateur la coche / décoche)
    // Liste des données des objets Port de la couche
    orderedPortsDataList=new Array();  // Pour tous affichages de données
    orderedDataListForPortNames=new Array();   // pour l'affichage des noms des ports seulement (en respectant leur importance, pour la collision des étiquettes de noms)
    // Stats sur les données de la couche (à partir de la liste des données)
    // Congés :
    moyenneConges=0;
    maxConges=0;
    // Variables :
    effectif=[0,0,0,0,0];   // Nb. de ports dans chaque tranche de 20%

    // Membres de la classe :
    // Constantes pour les id des couches :
    static L_CONGES=0;
    static L_VARIABLE=1;
    static L_LIM_AMIRAUTES=2;
    
    

    // Constructeur
    constructor(id, name, visible, isPortsLayer) {
        this.id=id;
        this.name=name;
        this.visible=visible;
        this.isPortsLayer=isPortsLayer;
    }

    // Mettre en cache dans l'objet PortData, la variable permettant le tri des Ports (selon la couche concernée)
    cacheValueToDisplay(objPortData, variable, source, annee) {
        if(variable == "conges") {  // Calcul du nombre de congés du port
            objPortData.computeNbConges(source, annee);
        } else {    // Calcul du pourcentage de pointcalls avec 'variable' renseignée
            objPortData.computeVariable(variable, source, annee);
        }
    }

    // Tri de la liste des données des ports, par ordre décroissant
    sortPortsDataList(destination, variable) {
        var fctPourTri=null;

        // Chercher la fonction à utiliser pour faire le tri :
        if(variable == "conges") {  // Tri par nb. de congés décroissant
            fctPourTri=PortData.compareConges;
        } else {    // Tri par valeur décroissante de la variable à afficher
            fctPourTri=PortData.compareVariable;
        }
        
        // Effectuer le tri :
        if(fctPourTri) {    // S'il y a un tri à faire
            destination.sort(fctPourTri);
        }
    }

    // Grouper les lignes de données par port, et trier la liste des ports par ordre décroissant de la valeur à afficher :
    prepareOrderedPortsDataList = (portsDataFromAPI, variable, source, annee, destination=this.orderedPortsDataList) => {
        var ogc_fid_prec=-1;    // id du port précédent, pour détecter un nouveau port
        var portData=new Array();   // tableau des données d'un port

        if(destination.length == 0) { // Construction initiale de la liste
            // Parcourir le tableau des données chargées, pour isoler les données de chaque port (ordonnées par : port, source, année) :
            portsDataFromAPI.forEach(item => {
                if(item.ogc_fid != ogc_fid_prec) {    // Données pour un nouveau port
                    // Mémoriser le port précédent :
                    if(portData.length > 0)   {
                        var objPortData=new PortData(portData); // Instancier une classe PortData
                        this.cacheValueToDisplay(objPortData, variable, source, annee);   // Mettre en cache une valeur permettant de trier la liste
                            // Pour mettre en cache dans l'objet la dernière valeur calculée (pour le tri de la liste des ports)
                            // ex.: objPortData.computeNbConges(source, annee);  
                        destination.push(objPortData);    // Ajout de l'objet PortData à la liste des ports
                    }
                    // Préparer un nouveau port :
                    portData=new Array();
                }
                // mémoriser la ligne de données :
                portData.push(item);
                ogc_fid_prec=item.ogc_fid;
            })
            // Mémoriser le dernier port :
            if(portData.length > 0)   {
                var objPortData=new PortData(portData); // Instancier une classe PortData
                this.cacheValueToDisplay(objPortData, variable, source, annee);   // Mettre en cache une valeur permettant de trier la liste
                    // Pour mettre en cache dans l'objet la dernière valeur calculée (pour le tri de la liste des ports)
                    // ex.: objPortData.computeNbConges(source, annee);  
                destination.push(objPortData);    // Ajout de l'objet PortData à la liste des ports
            }

            // Debug : affichage de la liste des ports (par ordre alphabétique, avant le tri par nb. de congés)
            /*
            console.log("\n\n\n"+destination.length+" ports :")
            destination.forEach(port => {
                // Affichage des lignes de données d'un port :
                port.dump();
                // Séparation des ports :
                console.log("-----")
            })
            */

        } else {    // Seulement recalculer la valeur servant à faire le tri
            destination.forEach(objPortData => {
                this.cacheValueToDisplay(objPortData, variable, source, annee);   // Mettre en cache une valeur permettant de trier la liste
                    // Pour mettre en cache dans l'objet la dernière valeur calculée (pour le tri de la liste des ports)
                    // ex.: objPortData.computeNbConges(source, annee);  
            })
        }

        // Tri des ports par nb. décroissant de : ex. congés (les plus gros ports en premier - pour gestion de collision des étiquettes de nom)
        this.sortPortsDataList(destination, variable);   // ex.: utiliser la dernière valeur de computeNbConges() mise en cache dans chaque objet pour le tri décroissant
    }

    // Calculs de statistiques pour affichage dans la légende de la couche
    computeDataForLegend() {
        switch(this.id) {
            case Layer.L_CONGES :   // Calculer valeurs moyenne et max. des congés
                if(this.orderedPortsDataList) { // Les données sont prêtes
                    const nbPorts=this.orderedPortsDataList.length;
                    // Val. max des congés : la liste vient d'être triée par ordre décroissant des congés, il suffit de prendre le nb. de congés du 1er port
                    this.maxConges=0;
                    if(nbPorts > 0) {
                        const firstObjPortData=this.orderedPortsDataList[0];
                        this.maxConges=firstObjPortData.lastNbCongesComputed;
                    }
                    // Val. moyenne des congés :
                    // Ne pas prendre en compte les ports n'ayant aucune donnée (lastNbCongesComputed à 0)
                    this.moyenneConges=0;
                    var sommeConges=0;
                    var nbPortsAvecConges=0;
                    if(nbPorts > 0) {
                        nbPortsAvecConges=(this.orderedPortsDataList[0].lastNbCongesComputed == 0) ? 0 : 1; // Doit-on compter le premier port de la liste
                        sommeConges=this.orderedPortsDataList.reduce((a, b) => { 
                            if(b.lastNbCongesComputed != 0) {   // On doit compter ce port
                                nbPortsAvecConges++;
                            }
                            return (((typeof a) == "object") ? a.lastNbCongesComputed : a) + b.lastNbCongesComputed; 
                        });
                        if(nbPortsAvecConges != 0) {
                            this.moyenneConges=Math.round(sommeConges/nbPortsAvecConges);
                        }
                    }
                }
                break;
            case Layer.L_VARIABLE : // Compter le nb. de ports dans chaque tranche de 20%
                this.effectif=[0,0,0,0,0];  // RAZ compteurs
                if(this.orderedPortsDataList) { // Les données sont prêtes
                    this.orderedPortsDataList.forEach(portData => {
                        const [total, variableValue]=portData.lastVariableComputed; // récupérer valeur juste calculée
                        if(total > 0) { // Il y a des données pour ce port
                            if(variableValue <= 20) {
                                this.effectif[0]++;
                            }
                            if((variableValue > 20)&&(variableValue <= 40)) {
                                this.effectif[1]++;
                            }
                            if((variableValue > 40)&&(variableValue <= 60)) {
                                this.effectif[2]++;
                            }
                            if((variableValue > 60)&&(variableValue <= 80)) {
                                this.effectif[3]++;
                            }
                            if(variableValue > 80) {
                                this.effectif[4]++;
                            }
                        }
                    })    
                }
                break;
        }
    }

}