// ListLayers.js
/**
 * Liste des couches de la carte, sous la forme : {id, name, visible}
 * Hérite de : Array (tableau)
 */

import { Layer } from './Layer'

export class ListLayers {
    // données membres :
    layers=null;    // Tableau d'objets Layer
    
    // Constructeur : on passe un tableau d'objets Layer pour remplir la liste
    constructor(tabLayers) {
        this.layers=tabLayers;
    }

    // Retrouve une couche de la liste par son id, renvoie undefined si introuvable
    getLayerById(id) {
        return this.layers.find(element => (element.id == id));
    }

    // Retrouve une couche de la liste par son nom (name), renvoie undefined si introuvable
    getLayerByName(name) {
        return this.layers.find(element => (element.name == name));
    }

    // Retrouve la couche ayant les noms des ports (isPortsLayer à true), en fonction de la variable qu'elle affiche (congés ou var. de complétude)
    getPortsLayerByVariable(variable) {
        return this.layers.find(layer => {
            var result=false    // à priori

            // Vérifier si on a trouvé la bonne couche :
            if(layer.isPortsLayer) {    // Couche ayant les noms des ports
                if( ((variable == "conges")&&(layer.id == Layer.L_CONGES)) || ((variable != "conges")&&(layer.id == Layer.L_VARIABLE)) ) {  // couche correspondant à la variable affichée
                    result=true // couche trouvée
                }
            }
            return result
        })
    }

    // Renvoie les couches visibles de la liste
    getVisibleLayers() {
        return this.layers.filter(layer => layer.visible);
    }

    // Mémorise toutes les couches comme masquées
    setAllLayersInvisible(idExceptionList) {
        this.layers.forEach(layer => {
            if(!idExceptionList.includes(layer.id)) {
                layer.visible=false;
            }
        })
    }

    



}