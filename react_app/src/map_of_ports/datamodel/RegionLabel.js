// RegionLabel.js
/**
 * Classe contenant les données nécessaires au positionnement des noms de régions sur une carte
 * Tri par taille décroissante prévu, pour gérer la collision des étiquettes lors du zoom / dézoom
 */

import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js';
 
export class RegionLabel {
    // Champs mémorisés :
    id=0;   // entier unique dans la liste
    name="";    // nom de la région, ex.: "Poitou"
    latLng=null;    // Coordonnées pour placer l'étiquette sur la carte du monde en 1789. De type : classe LatLng (Leaflet)
    bounds=null;    // Rectangle englobant de la région. De type : classe LatLngBounds (Leaflet)
    // Champs calculés :
    area=0.0;   // Valeur proportionnelle à l'aire de la bounding box (bounds)

    // Calcul d'aire d'un objet de classe LatLngBounds
    computeArea(bounds) {
        const northEast=bounds.getNorthEast();
        const southWest=bounds.getSouthWest();
        const x1=southWest.lat;
        const x2=northEast.lat;
        const y1=northEast.lng;
        const y2=southWest.lng;
        const aire=Math.abs(x2-x1)*Math.abs(y2-y1);
        return aire;
    }
 
    // Constructeur
    constructor(id, name, latLng, bounds) {
        this.id = id;
        this.name = name;
        this.latLng = latLng;
        this.bounds = bounds;
        // Calcul de la pseudo-aire (taille) de la région :
        this.area=this.computeArea(this.bounds);
    }
 
    // Fonction RegionLabel.fonctionComparaison à passer à Array.sort() pour trier les régions par ordre de taille (les plus grosses régions en premier)
    static fonctionComparaison(a, b) {
        return (b.area - a.area); // si b > a (en surface), résultat positif : b classé avant a
    }
 
    // Propriété id : id (entier) de la région
    get id() {
        return this.id;
    }
 
    // Propriété name : Nom de la région (pour affichage)
    get name() {
        return this.name;
    }
 
    // Propriété point : Point Leaflet de coordonnées de placement de l'étiquette (ex.: L.latLng(50.5, 30.5))
    get point() {
        return this.latLng;
    }

    // Propriété area : grandeur proportionnelle à la taille de la région (pseudo-aire de son rectangle englobant)
    get area() {
        return this.area;
    }

};