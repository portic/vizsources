// Source.js
/**
 * Une source (G5, Marseille, Petit cabotage, départs au long cours)
 * avec son nom dans la base de données (données provenant de l'API), et son nom à afficher, ...
 * 
 * translate prefix : controlsViz (src_...)
 */

import React from 'react';
import i18n from '../../translate/i18nextInit';
import { Form } from 'react-bootstrap';

export class Source {
    // Données de l'instance :
    sourceInfo=null

    // Données de classe :

    // Tableau des vrais noms des sources (tels que reçus de l'API - pour gérer les futurs changements)
    static tabRealSourcesNames=[
        { abbrev: "g5", trueName: "G5", displayNameKey: 'controlsViz.src_g5', displayOrder: 1, years: ["1787", "1789"] },
        { abbrev: "marseille", trueName: "la Santé registre de patentes de Marseille", displayNameKey: 'controlsViz.src_sante_marseille', displayOrder: 2, years: ["1749", "1759", "1769", "1779", "1787", "1789", "1799"] },
        { abbrev: "cabotage", trueName: "Registre du petit cabotage (1786-1787)", displayNameKey: 'controlsViz.src_petit_cabotage', displayOrder: 3, years: ["1787", "1789"] },
        { abbrev: "expeditions", trueName: "Expéditions coloniales Marseille (1789)", displayNameKey: 'controlsViz.src_expeditions', displayOrder: 4, years: ["1789"] }
    ];
    // Rq.: la liste des années possibles est dans le tableau UrlParamsParser.anneesOk

    // Pour retrouver un nom de source par un entier (constante) :
    static INDEX_G5=0;
    static INDEX_MARSEILLE=1;
    static INDEX_CABOTAGE=2;
    static INDEX_EXPEDITIONS=3;

    // Constructeur (par nom trouvé dans les données de l'API)
    constructor(sourceNameFromAPI) {
        this.sourceInfo=this.getSourceInfo(sourceNameFromAPI)   // recherche les infos sur la source dont on passe le nom
    }

    // Renvoie l'élément du tableau tabRealSourcesNames correspondant à cet objet
    getSourceInfo(sourceNameFromAPI) {
        var result=null;
        
        const found=Source.tabRealSourcesNames.find(element => element.trueName == sourceNameFromAPI); // recherche de sourceNameFromAPI dans la table : tabRealSourcesNames
        if(!(found == undefined)) { // On a trouvé une correspondance
            result=found;  // Renvoyer le vrai nom de la source
        }
        return result;
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les sources dans l'ordre d'affichage souhaité
    static compareByDisplayOrder(a, b) {
        return a.displayOrder-b.displayOrder    // Tri par indice d'ordre d'affichage des sources
    }

    // Retourne un tableau d'objets Source, avec toutes les sources de tabRealSourcesNames, dans le bon ordre d'affichage
    static getAllSources() {
        const result=new Array()
        var newSource=null

        // Parcourir le tableau statique des infos sur les sources de Portic
        Source.tabRealSourcesNames.forEach(sourceInfo => {  // Pour chaque source :
            newSource=new Source(sourceInfo.trueName)   // Instancier la source, à partir de son nom dans l'API (argument du constructeur)
            result.push(newSource)  // Ajout à la liste
        })
        // Trier la liste par ordre d'affichage :
        result.sort(Source.compareByDisplayOrder)
        return result
    }

    // Renvoie le nom court pour la source (employé pour les listes déroulantes, les key, ...)
    get abbrev() {
        var result=""

        if(this.sourceInfo) {    // Info trouvée
            result=this.sourceInfo.abbrev  // Retourner le nom court de la source
        }
        return result
    }

    // Renvoie le nom interne à l'API pour la source
    get trueName() {
        var result=""

        if(this.sourceInfo) {    // Info trouvée
            result=this.sourceInfo.trueName  // Retourner le nom d'affichage de la source
        }
        return result
    }

    // Renvoie la chaine à afficher pour la source
    get displayName() {
        var result=""

        if(this.sourceInfo) {    // Info trouvée
            const key=this.sourceInfo.displayNameKey   // clé de traduction du nom d'affichage de la source
            result=i18n.t(key)  // Retourner le nom d'affichage (traduit) de la source
        }
        return result
    }

    // Renvoie N° d'ordre d'affichage pour la source
    get displayOrder() {
        var result=Number.NaN

        if(this.sourceInfo) {    // Info trouvée
            result=this.sourceInfo.displayOrder  // Retourner l'ordre d'affichage de la source
        }
        return result
    }

    // Renvoie un tableau des années (chaînes de car.) pour lesquelles on a des données dans cette source
    get years() {
        var result=[]

        if(this.sourceInfo) {    // Info trouvée
            result=this.sourceInfo.years  // Retourner le tableau des années dispo (chaines de car.) pour cette source
        }
        return result
    }

    /**
     * Renvoie l'objet Form.Check de React-Bootstrap, pour construire le groupe de boutons "source" (name), représentant un bouton radio avec son displayName, ...
     * @param {string} selectedSourceAbbrev La variable contenant la source actuellement sélectionnée, sous forme d'abbréviation ("g5" ou "marseille" ou ...)
     * @param {function} handleChangeFunction Fonction gérant l'événement d'un changement de sélection, dans le formulaire contenant le bouton radio retourné
     * @returns {Form.Check} Le composant React (jsx) représentant la source (cette instance), sous forme de bouton radio. Le bouton radio fait partie d'un groupe nommé "source" (propriété "name")
     */
    getRadioButton(selectedSourceAbbrev, handleChangeFunction) {
        const abbrev=this.abbrev    // ex.: "g5", "marseille", ...
        const id="source_"+abbrev
        const result=<Form.Check 
            type={"radio"} 
            name={"source"}
            id={id} 
            key={id} 
            label={this.displayName} 
            value={abbrev} 
            checked={selectedSourceAbbrev == abbrev} 
            onChange={handleChangeFunction} 
        />

        return result
    }

    /**
     * Renvoie le tag html <option> comme composant jsx (pour liste déroulante Form.Select de React-Bootstrap), pour l'année demandée.
     * L'option de la liste déroulante est grisée (disabled) si l'année n'est pas disponible pour la source.
     * @param {string} yearStr L'année demandée
     * @returns {HTMLOptionElement} Le composant React <option> (jsx) représentant l'année demandée
     */
    getSelectOptionForYear(yearStr) {
        const abbrev=this.abbrev    // ex.: "g5", "marseille", ...
        const keyValue=abbrev+"_"+yearStr
        const result=<option 
            key={keyValue} 
            value={yearStr} 
            disabled={!this.years.includes(yearStr)} 
        >{yearStr}</option>

        return result
    }

    // renvoie le vrai nom d'une source dans les données, repérée par son nom abrégé (venant d'un bouton radio ou d'une liste déroulante par ex.)
    static getRealSourceName(abbrev) {
        var result=abbrev;  // Si la source n'a pas de vrai nom, renvoyer l'abbréviation (par ex. dans le cas de 'all')
        
        const found=Source.tabRealSourcesNames.find(element => element.abbrev == abbrev); // recherche de abbrev dans la table : tabRealSourcesNames
        if(!(found == undefined)) { // On a trouvé une correspondance
            result=found.trueName;  // Renvoyer le vrai nom de la source
        }
        return result;
    }

    // renvoie le vrai nom d'une source dans les données, repérée par un N° d'index (pour repérage par une constante numérique)
    static getRealSrcNameByIndex(index) {
        return Source.tabRealSourcesNames[index].trueName;
    }



 }