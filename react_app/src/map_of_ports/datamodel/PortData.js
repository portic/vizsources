// PortData.js
/**
 * Classe de gestion des données d'un port
 * Peut être mise en liste dans un tableau
 * Contient une méthode statique de tri des ports, par ordre de congés décroissants, pour gérer correctement les collisions des étiquettes de nom
 * Les lignes de données (tableau dataLines) viennent de l'API Portic
 * ( Syntaxe des classes JavaScript - Voir : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Classes )
 */

import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js';
import React from 'react';
import { Source } from './Source'

export class PortData {
    // Lignes de données issues de l'API Portic :
    dataLines=null;

    // Mémorise la dernière valeur de congé calculée (pour le tri d'une liste, sans passer de paramètre)
    lastNbCongesComputed=-1;

    // Mémorise la dernière valeur calculée d'une variable (pour le tri d'une liste, sans passer de paramètre)
    lastVariableComputed=[-1,0,0];    // [total (pointcalls), renseignés (%), part déduite des renseignés (%)]


    // Constructeur
    constructor(dataLines) {
        this.dataLines = dataLines;
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les ports par ordre décroissant du de nb. de congés (les plus gros ports en premier)
    static compareConges(a, b) {
        const nbCongesA=a.lastNbCongesComputed;
        const nbCongesB=b.lastNbCongesComputed;
        return (nbCongesB - nbCongesA); // si b > a (en nombre de congés), résultat positif : b classé avant a
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les ports par ordre décroissant de la variable (les plus gros ports en premier)
    static compareVariable(a, b) {
        const varValueA=a.lastVariableComputed[0];  // total
        const varValueB=b.lastVariableComputed[0];
        return (varValueB - varValueA); // si b > a (en valeur de la variable), résultat positif : b classé avant a
    }

    

    // Propriété ogc_fid : id (entier) du port
    get ogc_fid() {
        return this.dataLines[0].ogc_fid;
    }

    // Propriété uhgs_id : id (txt) du port
    get uhgs_id() {
        return this.dataLines[0].uhgs_id;
    }

    // Propriété name : Nom du port (pour affichage)
    get name() {
        return this.dataLines[0].toponym;
    }

    // Propriété point : Point Leaflet de coordonnées du port (ex.: L.latLng(50.5, 30.5))
    get point() {
        // Extraction des coordonnées (point) du port :
        const geoJSONfeaturePoint=JSON.parse(this.dataLines[0].point);
        const point=L.GeoJSON.coordsToLatLng(geoJSONfeaturePoint.coordinates);
        return point;
    }

    // Statut : rien, oblique, siège d'amirauté
    get status() {
        return this.dataLines[0].status;
    }

    // Indique (true/false) s'il y a un greffier au port
    get hasAClerk() {
        return this.dataLines[0].has_a_clerk;
    }

    // Admiralty
    get admiralty() {
        return this.dataLines[0].admiralty;
    }

    // Province
    get province() {
        return this.dataLines[0].province;
    }


    // Calcul de nb_conges :
    // recherche dans this.dataLines avec paramètres (source -> source_suite) et (annee -> pointcall_year)
    // retourne un tableau [nombre de congés pour ce port - 0 si données à null, true si inputdone - false si cr]
    computeNbConges(sourceAbbrev, annee) {
        var resNbConges=0;
        var resIsInputdone=false;
        
        const source=Source.getRealSourceName(sourceAbbrev);  // retrouve le vrai nom dans les données de la source demandée (ex.: 'marseille' -> 'Santé Marseille')
        this.dataLines.forEach(item => {
            
            
            // Voir si on prend la ligne de données en compte :
            // - tester la source :
            const sourceSuite=item.source_suite;
            if( sourceSuite && ((sourceSuite == source ) || (source == "all")) ) {
                // - tester l'année :
                if(item.pointcall_year) {
                    const txtAnnee=item.pointcall_year.toString();
                    if((txtAnnee == annee) || (annee == "all")) {
                        // On prend en compte la ligne de données.
                        switch(sourceSuite) {
                            case Source.getRealSrcNameByIndex(Source.INDEX_G5) :
                                // Chercher si inputDone :
                                if(!item.nb_conges_inputdone) { // pas de valeur inputdone => on va regarder la valeur cr :
                                    // Mémo. valeur cr utilisée
                                    // (s'il y en a une) :
                                    if(item.nb_conges_cr) {
                                        resNbConges+=item.nb_conges_cr;
                                    }                        
                                } else {    // on a une valeur inputdone
                                    // Mémo. valeur inputdone utilisée :
                                    resIsInputdone=true;
                                    // utiliser valeur inputdone :
                                    resNbConges+=item.nb_conges_inputdone;
                                }
                                break;
                            case Source.getRealSrcNameByIndex(Source.INDEX_MARSEILLE) :
                                if(item.nb_conges_sante) {
                                    resNbConges+=item.nb_conges_sante;
                                    resIsInputdone=true;
                                }
                                break;
                            case Source.getRealSrcNameByIndex(Source.INDEX_CABOTAGE) :
                                if(item.nb_petitcabotage) {
                                    resNbConges+=item.nb_petitcabotage;
                                    resIsInputdone=true;
                                }
                                break;
                            case Source.getRealSrcNameByIndex(Source.INDEX_EXPEDITIONS) :
                                if(item.nb_longcours_marseille) {
                                    
                                    resNbConges+=item.nb_longcours_marseille;
                                    resIsInputdone=true;
                                }
                                break;
                        }
                        
                    }
                }
            }
        });
        this.lastNbCongesComputed=resNbConges;  // mémo. dernière valeur calculée, pour tri sans paramètres
        const aRetourner=[resNbConges, resIsInputdone];
        return aRetourner;
    }

    
    /**
     * Pour chaque variable demandée, renvoit un tableau de 3 valeurs :
     * - Nombre total de pointcalls (pour la taille du cercle)
     * - Pourcentage de pointcalls ayant la variable renseignée, par rapport au nombre total de pointcalls (pour la couleur de remplissage du cercle)
     * - Pourcentage des valeurs qui sont déduites, dans le nombre de pointcalls ayant la variable renseignée (pour la part déduite)
     * 
     * @param {string} variable Nom de la variable qu'on souhaite calculer (valeurs possibles : "tonnage", "homeport", "products", "birthplace", "citizenship", "flag")
     * @param {string} sourceAbbrev Nom abrégé de la source dans laquelle on veut prendre les valeurs (ex.: "g5")
     * @param {string} annee Année pour laquelle on prend les données (ex.: "1789")
     * @returns [nb total de pointcalls, pourcentage de pointcalls ayant la variable demandée renseignée, pourcentage des pointcalls renseignés pour lesquels la valeur renseignée est déduite]
     */
    computeVariable(variable, sourceAbbrev, annee) {
        var resTotal=0;
        var resPourcentage=0;
        var resDeduced=0;
        var variableValue=0;
        var deducedValue=0;
        
        const source=Source.getRealSourceName(sourceAbbrev);  // retrouve le vrai nom dans les données de la source demandée (ex.: 'marseille' -> 'Santé Marseille')
        this.dataLines.forEach(item => {
            // Voir si on prend la ligne de données en compte :
            // - tester la source :
            const sourceSuite=item.source_suite;
            if( sourceSuite && (sourceSuite == source ) ) {
                // - tester l'année :
                if(item.pointcall_year) {
                    const txtAnnee=item.pointcall_year.toString();
                    if(txtAnnee == annee) {
                        // On prend en compte la ligne de données

                        // Calcul du pourcentage :
                        // total : on calcule le pourcentage par rapport au nb. total de pointcalls
                        resTotal=0;    // par défaut
                        if(!item.total) {   // total inconnu : calcul du pourcentage impossible => 0
                            variableValue=0;
                            resPourcentage=0;
                            resDeduced=0;
                        } else {    // total connu
                            resTotal=item.total;

                            // valeur de la variable demandée :
                            variableValue=0;    // par défaut
                            switch(variable) {
                                case "tonnage" :
                                    variableValue=item.nb_tonnage_filled;
                                    deducedValue=item.nb_tonnage_deduced;
                                    break;
                                case "homeport" :
                                    variableValue=item.nb_homeport_filled;
                                    deducedValue=item.nb_homeport_deduced;
                                    break;
                                case "products" :
                                    variableValue=item.nb_product_filled;
                                    deducedValue=0; // Pas de valeur déduite pour les produits
                                    break;
                                case "birthplace" :
                                    variableValue=item.nb_birthplace_filled;
                                    deducedValue=item.nb_birthplace_deduced;
                                    break;
                                case "citizenship" :
                                    variableValue=item.nb_citizenship_filled;
                                    deducedValue=item.nb_citizenship_deduced;
                                    break;
                                case "flag" :
                                    variableValue=item.nb_flag_filled;
                                    deducedValue=item.nb_flag_deduced;
                                    break;
                            }

                            // pourcentage :
                            resPourcentage=(variableValue/resTotal)*100;
                            // resPourcentage=Math.round(resPourcentage);  // Arrondis à l'entier le plus proche
                            // part déduite :
                            if(variableValue == 0) {    // La variable n'est renseignée pour aucun pointcall
                                resDeduced=0
                            } else {    // Des pointcalls ont une valeur de la variable renseignée => on peut calculer le pourcentage de valeurs déduites
                                resDeduced=(deducedValue/variableValue)*100;    // Ne pas arrondir, pour afficher 2 chiffres significatifs max.
                            }
                        }

                    }
                }
            }
        });

        const aRetourner=[resTotal, resPourcentage, resDeduced];
        this.lastVariableComputed=aRetourner;   // mémo. dernière valeur calculée, pour tri sans paramètres
        return aRetourner;
    }

    // renvoie un élément jsx affichable, pour le tableau présenté dans le tooltip au survol du port à la souris
    getCongesFor(source, annee) {
        var result=<></>;

        this.dataLines.forEach(item => {
            // Voir si on prend la ligne de données en compte :
            // - tester la source :
            const sourceSuite=item.source_suite;
            if( sourceSuite && (sourceSuite == source ) ) {
                // - tester l'année :
                const valAnnee=item.pointcall_year;
                if( item.pointcall_year && (valAnnee == annee) ) {
                    // On prend en compte la ligne de données.
                    switch(source) {
                        case Source.getRealSrcNameByIndex(Source.INDEX_G5) :
                            const inputDone=item.nb_conges_inputdone;
                            const cr=item.nb_conges_cr;
                            result=<span>
                                <strong>{inputDone === null ? "-" : inputDone}</strong> / {cr === null ? "-" : cr}
                            </span>
                            break;
                        case Source.getRealSrcNameByIndex(Source.INDEX_MARSEILLE) :
                            const nbCongesSante=item.nb_conges_sante;
                            result=<span>{nbCongesSante === null ? "-" : nbCongesSante}</span>
                            break;
                        case Source.getRealSrcNameByIndex(Source.INDEX_CABOTAGE) :
                            const nbPetitCabotage=item.nb_petitcabotage;
                            result=<span>{nbPetitCabotage === null ? "-" : nbPetitCabotage}</span>
                            break;
                        case Source.getRealSrcNameByIndex(Source.INDEX_EXPEDITIONS) :
                            const nbExpeditions=item.nb_longcours_marseille;
                            result=<span>{nbExpeditions === null ? "-" : nbExpeditions}</span>
                            break;
                    }   
                }
            }
        })
        return result;
    }

    /**
     * Calcul de tonnage :
     * recherche dans this.dataLines avec paramètres (source -> source_suite) et (annee -> pointcall_year)
     * retourne dans un tableau de deux nombres :
     * - la valeur cumulée des lignes de données demandées
     * - la part déduite de catte valeur (en %)
     * @param {string} sourceAbbrev Nom abrégé de la source, ou "all" si on souhaite agréger toutes les sources
     * @param {string} annee L'année pour laquelle on veut le tonnage, ou "all" pour agréger les années
     * @returns [tonnage en tonneaux (int or null), pourcentage déduit (float defaut 0.0)]
     */
    computeTonnage(sourceAbbrev, annee) {
        var resTonnage=null;
        var resTonnageDeduced=null;
        var tonnageDeducedPercent=0.0
        
        const source=Source.getRealSourceName(sourceAbbrev);  // retrouve le vrai nom dans les données de la source demandée (ex.: 'marseille' -> 'Santé Marseille')
        this.dataLines.forEach(item => {
            // console.log(item.toponym+" ("+source+", "+annee+") : "+item.good_sum_tonnage+" ("+item.source_suite+", "+item.pointcall_year+")")
            // Voir si on prend la ligne de données en compte :
            // - tester la source :
            const sourceSuite=item.source_suite;
            if( sourceSuite && ((sourceSuite == source ) || (source == "all")) ) {
                // - tester l'année :
                if(item.pointcall_year) {
                    const txtAnnee=item.pointcall_year.toString();
                    if((txtAnnee == annee) || (annee == "all")) {
                        // On prend en compte la ligne de données.
                        if(item.good_sum_tonnage) {
                            resTonnage+=item.good_sum_tonnage;
                            resTonnageDeduced+=item.good_sum_tonnage_deduced;
                        }
                        
                    }
                }
            }
        });
        if(resTonnage) {    // On a trouvé l'info
            // Calcul du pourcentage déduit :
            if(resTonnageDeduced && (resTonnage != 0)) {    // Le calcul de la part déduite est possible
                tonnageDeducedPercent=(resTonnageDeduced/resTonnage)*100.0  // Pas d'arrondis pour affichage avec 2 digits
            }
            // Arrondis du tonnage trouvé :
            resTonnage=Math.round(resTonnage);  // Arrondis à l'entier le plus proche
        }
        return [resTonnage, tonnageDeducedPercent];
    }

    // Cherche la liste des sources disponibles pour le port
    get availableSources() {
        const sourceNames=new Array()   // Tableau d'objets Source
        const sources=new Array()   // tableau des noms des sources déjà trouvées

        // Remplissage des listes :
        this.dataLines.forEach(item => {
            const sourceName=item.source_suite;
            if(sourceName) {
                if(!sourceNames.includes(sourceName)) {  // source pas déjà dans la liste
                    // l'ajouter :
                    sourceNames.push(sourceName)    // ajouter le nom
                    const newSource=new Source(sourceName)
                    sources.push(newSource);    // ajouter l'objet
                }
            }
        })
        // Tri de la liste des sources, dans l'ordre d'affichage :
        sources.sort(Source.compareByDisplayOrder)
        return sources;
    }

    // Cherche la liste des années pour lesquelles on a des données pour le port
    get availableYears() {
        const tabStrAnnees=new Array()   // Tableau des années (chaines de car.)

        // Remplissage de la liste :
        this.dataLines.forEach(item => {
            const year=item.pointcall_year;
            var strYear=null

            if(year) {
                strYear=year.toString() // année en chaine de car.
                if(!tabStrAnnees.includes(strYear)) {  // année pas déjà dans la liste
                    // l'ajouter :
                    tabStrAnnees.push(strYear)    // ajouter l'année
                }
            }
        })
        // Tri de la liste des années :
        tabStrAnnees.sort()
        return tabStrAnnees;
    }


    // Affichage des lignes de données contenues, pour debug
    dump() {
        // Affichage des lignes de données d'un port :
        console.log(this);
        this.dataLines.forEach(ligne => {
            // console.log(ligne.ogc_fid.toString()+": "+ligne.toponym+" "+ligne.source_suite+" "+ligne.pointcall_year)
            console.log(ligne.toponym+": src: "+ligne.source_suite+" an: "+ligne.pointcall_year+" inpDn: "+ligne.nb_conges_inputdone+" cr: "+ligne.nb_conges_cr)
        })
    }

};
  