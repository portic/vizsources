// UpdateMapParameters.jsx
/**
 * Composant qui met à jour, dans l'état de VizSources, les valeurs de 'center', 'zoom', 'world' (fond de carte choisi)
 * Ce composant non-visuel doit être enfant de MapContainer (situé dans MapOfFrenchPorts), pour s'abonner aux événements Leaflet : zoomend et moveend
 */

import {useMapEvents} from "react-leaflet"
import { useTranslation } from 'react-i18next';
import { UrlParamsParser } from "../UrlParamsParser";

export const UpdateMapParameters=({changeCenter, changeZoom, changeWorld}) => {
    const { t } = useTranslation()

    /**
     * Retrouve le nom abrégé (world) de la couche de base de la carte, à partir de son nom d'affichage (selon la langue sélectionnée)
     * Utilise la table statique UrlParamsParser.worldDisplayNameKeys : clé de traduction (nom d'affichage) <-> world (paramètre d'URL)
     * @param {string} baseLayerDisplayName Le nom de la couche de base de Leaflet (fond de carte), tel qu'il est affiché dans la langue sélectionnée
     * @returns {string} world, la valeur abrégée utilisée dans UrlParamsParser.js pour désigner le fond de carte sélectionné
     */
    const getWorldFromDisplayName = (baseLayerDisplayName) => {
        var result=null;
        
        const found=UrlParamsParser.worldDisplayNameKeys.find(element => t(element.layerDisplayNameKey) == baseLayerDisplayName); // recherche de sourceNameFromAPI dans la table : tabRealSourcesNames
        if(!(found == undefined)) { // On a trouvé une correspondance
            result=found.world;  // Renvoyer le vrai nom de la source
        }
        return result;
    }

    // Pour mémoriser les valeurs des paramètres, enregistrer des gestionnaires d'événements : 
    // ( liste des événements : https://leafletjs.com/reference.html#map-event )
    const map = useMapEvents({
        // Layer events :
        baselayerchange: (layersControlEvent) => {    // Changement de fond de carte (événement de type LayersControlEvent, voir : https://leafletjs.com/reference.html#layerscontrolevent )
            // console.log("baselayerchange")
            const baseLayerDisplayName=layersControlEvent.name  // récupérer dans l'événement, le nom du fond de carte nouvellement coché
            const world=getWorldFromDisplayName(baseLayerDisplayName)   // chercher le nom abrégé correspondant (dans la liste des valeurs du paramètre 'world' de l'URL de l'application)
            if(world) { // Correspondance trouvée
                changeWorld(world)  // mémoriser la nouvelle valeur dans l'état de la classe de base de l'application (VizSources.jsx)
                // console.log("world: "+world)
            }
        },

        // Map state change events :
        zoomend: () => {    // Niveau de zoom
            // console.log("zoomend");
            const zoomLevel=map.getZoom()
            changeZoom(zoomLevel)
        },     // Zoom avant ou arrière terminé
        moveend: () => {    // Centre de la carte
            // console.log("moveend");
            const centerCoords=map.getCenter()
            changeCenter(centerCoords)
        },  // modification du Center terminée
    })

    // Ce composant n'affiche rien :
    return null
}





