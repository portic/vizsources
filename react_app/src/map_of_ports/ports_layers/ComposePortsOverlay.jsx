// ComposePortsOverlay.jsx
/**
 * Contient la fonction créant un HOC servant à produire une couche de données de carte, à partir d'une fonction de représentation graphique d'un port.
 * 
 * HOC (high order component) = fonction qui compose un composant de base (et d'éventuels paramètres), et renvoie un autre composant avec plus de fonctionnalités.
 * 
 * Ce HOC prend une représentation de port (ex.: PortConges) et renvoie une couche de carte (overlay / LayerGroup) contenant : 
 * - les ports, représentés à partir du composant passé en paramètre, ajoutés dans l'ordre décroissant de la variable sélectionnée ;
 * - les étiquettes des noms des ports, ajoutées par ordre de nb. de congés décroissant (source: all, année: all - pour détection de collision des étiquettes)
 * 
 * ex. d'utilisation : 
 * - création : const PortsCongesOverlay = composePortsOverlay(PortConges);
 * - utilisation du HOC créé : <PortsCongesOverlay
 *                                  layerData={lConges} 
 *                                  portsData={portsDataFromAPI} 
 *                                  variable={displayVariable} source={source} annee={annee} 
 *                                  whenReady={() => {!allLayersReady && setAllLayersReady(true)}} 
 *                              />
 * 
 */

import React, { useEffect, useState } from 'react';
import { LayersControl, LayerGroup } from 'react-leaflet'
import { useTranslation } from 'react-i18next';
import { PortNames } from './PortNames'
import { PortData } from '../datamodel/PortData'

// Fonction renvoyant le HOC :
// à appeler, juste après l'import, pour créer le HOC (la couche de carte), en passant la représentation d'un port à utiliser (PortDisplayComponent)
export const composePortsOverlay = (PortDisplayComponent) => {

    // Le HOC :
    return ({layerData, portsData, variable, source, annee, whenReady }) => {
        const { t } = useTranslation();

        // State :
        // * layerIsReady : pour déclencher une mise à jour du composant parent par whenReady()
        const [layerIsReady, setLayerIsReady]=useState(false);  // Pour attendre la fin de la création des ports
        // * displayVariable : variable de l'état, miroir de 'variable' mais différée, pour garantir une mise à jour correcte des couches lors de son changement
        const [displayVariable, setDisplayVariable] = useState(variable);

        // Données pour la détection de collision des noms de ports :
        useEffect(() => {
            // console.log("useEffect portNames "+displayVariable);
            // Préparation de la liste de données pour l'affichage correct des noms des ports :
            // Séparation données par port + tri initial par ordre décroissant des congés (toutes sources, toutes années) :
            layerData.prepareOrderedPortsDataList(portsData, "conges", "all", "all", layerData.orderedDataListForPortNames);    // vers la liste pour affichage des noms des ports
            // console.log(layerData.orderedDataListForPortNames);
        }, []); // Fait initialement, une seule fois

        useEffect(() => {
            // console.log("useEffect (variable, source, annee)");
            // Séparation données par port + tri décroissant initial :
            layerData.prepareOrderedPortsDataList(portsData, variable, source, annee);
            // Statistiques pour la légende de la couche :
            layerData.computeDataForLegend();
            // Mémo. valeur de 'variable' dans le state :
            setDisplayVariable(variable);
        }, [variable, source, annee]); // Fait initialement + changement des variables, pour ré-évaluer l'ordre d'affichage des ports

        // Signaler à l'appelant que la couche de carte est prête
        useEffect(() => {
            // console.log("useEffect (layerIsReady : "+layerIsReady+")");
            if(whenReady) { // Une fonction est fournie par l'appelant, on peut l'exécuter :
                whenReady();
                // console.log("whenReady()")
            }
        }, [layerIsReady, displayVariable]); // Fait initialement, et quand layerIsReady ou displayVariable changent

        // Rendu du composant :
        // console.log("render couche ports");

        // Rq.: lors d'un changement de 'variable', on a : variable <> displayVariable, ce qui enlève la couche du LayersControl,
        // et permet de la ré-insérer lors de la MAJ de displayVariable => c'est comme si on insérait une nouvelle couche.
        // => permet de changer le nom de la couche (name), qui est pourtant une propriété non-mutable !
        // console.log("displayVariable: "+displayVariable)

        return (<>
            { (variable == displayVariable) && 
                <LayersControl.Overlay 
                    checked={layerData.visible} 
                    // name={layerData.name} 
                    name={t('portsMap.layer_name_ports', "Port names")} 
                >
                    <LayerGroup>
                        { layerData.orderedPortsDataList && layerData.orderedPortsDataList.map(objPortData => (
                            <PortDisplayComponent 
                                data={objPortData} 
                                variable={displayVariable} source={source} annee={annee} 
                                key={objPortData.ogc_fid} />
                        ))}
                        { layerData.orderedDataListForPortNames && 
                            <PortNames 
                                layerData={layerData} 
                                classNameForPortNames={displayVariable} 
                            />
                        }
                    </LayerGroup>
                    { layerData.orderedDataListForPortNames && layerData.orderedPortsDataList && !layerIsReady && setLayerIsReady(true)}
                </LayersControl.Overlay>
            }
        </>);
    }

}