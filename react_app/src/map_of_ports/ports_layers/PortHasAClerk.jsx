// PortHasAClerk.jsx
/**
 * Composant d'affichage d'un port : sous forme de carré autour du point du port,
 * quand has_a_clerk vaut true dans les données.
 * Utilise le package Node : leaflet-geometryutil
 */

import React from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { Rectangle } from 'react-leaflet'
import '../../../css/tooltips-custom.css'
import { PortViewUtils } from './PortViewUtils'

export const PortHasAClerk = ({data}) => {
    // Mise en forme du carré :
    // Taille du marqueur (carré autour du point du port) :
    const squareSideInMeters=7*PortViewUtils.magnifyingFactor;

    const color_squareBorder='#8a2be2'   // BlueViolet
    // const color_squareBorder='#e31a1c'   // Rouge
    const squareMarkerLook={ stroke: true, color: color_squareBorder, opacity: 0.9, weight: 2, fill: false }    

    // Affichage du Port : si hasAClerk vaut true seulement
    const hasAClerk=data.hasAClerk;

    if(!hasAClerk) {    // ne rien afficher si pas de greffier au port
        return null;
    } else {    // Si greffier, afficher un carré autour du point du port
        const center=data.point;    // Centre du carré : le point du port
        const squareCoords=PortViewUtils.squareBounds(center, squareSideInMeters);    // bounds du carré (rectangle)

        return (
            <Rectangle
                bounds={squareCoords}
                pathOptions={squareMarkerLook}
            />
        )
    }
}