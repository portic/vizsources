// PortConges.jsx
/**
 * Composant d'affichage d'un port sous forme de cercle (d'aire proportionnelle au nombre de congés), avec nom du port
 * Le cercle est :
 * - vide si on a pris la valeur de nb_conges_cr, 
 * - plein si on a pris la valeur de nb_conges_inputdone
 * - un point noir si aucun congé
 * Un carré violet indique la présence d'un greffier dans le port :
 * - ce carré violet est affiché par le sous-composant : PortHasAClerk.jsx
 * 
 * Séparation des données et de la présentation :
 * - les données sont dans la prop : 'data', de classe PortData,
 * - la représentation (sous forme de marqueur cercle avec étiquette tooltip) est faite ici.
 */

import React from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { Tooltip, Circle } from 'react-leaflet'
import '../../../css/tooltips-custom.css'
import { PortCongesDataView } from './PortCongesDataView'
import { PortHasAClerk } from './PortHasAClerk'
import { PortViewUtils } from './PortViewUtils'
import { StripedCircle } from './StripedCircle'

export const PortConges = ({data, source, annee}) => {
    // Paramètres pour le marqueur :
    const radiusMin=3;
    const nbCongesMin=3;

    // Données du port : data, de classe PortData


    // Get radius in pixels of the CircleMarker
    const computeRadius = (source, annee) => {
        var result=2;   // Valeur par défaut
        
        const [nbConges, inputDone]=data.computeNbConges(source, annee);
        if(nbConges >= nbCongesMin) {  // pour les trop petites valeurs, renvoyer la valeur par défaut
            // Adapter la valeur à une plage affichable (marqueur de dimensions raisonnables) :
            // result=map(nbConges, nbCongesMin, nbCongesMax, radiusMin, radiusMax);   // changement de plage de valeurs

            // Calculer le rayon du marqueur cercle, pour que l'aire du marqueur soit proportionnelle au nombre de congés :
            result=PortViewUtils.radiusForCircleMarker(nbConges, nbCongesMin, radiusMin);
        }
        return result;
    }


    // Mise en forme du CircleMarker :
    const color_zeroConge='black'   // noir si pas de congé
    const color_Conges='#0095ff'    // bleu clair sinon
    const color_Stroke='blue'   // contour bleu foncé
    const opacity_Stroke=0.9
    const opacity_empty_marker=0.05
    const opacity_full_marker=0.8
    const width_Stroke=1
    // Cercle vide (conges_cr)
    const emptyMarkerLook={ stroke: true, color: color_Stroke, opacity: opacity_Stroke, weight: width_Stroke, fill: true, fillColor: color_Conges, fillOpacity: opacity_empty_marker }    
    // Cercle plein (conges_inputdone)
    const fullMarkerLook={...emptyMarkerLook, fillOpacity: opacity_full_marker}
    // Port sans congé (petit point noir)
    const markerNoCongeLook={color: color_zeroConge, stroke: false, fill: true, fillOpacity: 1}
    // Cercle invisible pour tooltip d'infos
    const markerInvisibleLook={ stroke: true, opacity: 0, weight: width_Stroke, fill: true, fillOpacity: 0}

    // Formatage du marqueur Circle : vide ou plein selon les données disponibles
    const computePathOptions = (source, annee) => {
        var result=emptyMarkerLook;  // Par défaut, cercle vide

        // Détecter le type de valeur affichée :
        const [nbConges, cerclePlein]=data.computeNbConges(source, annee);
        const pasDeConge=(nbConges < nbCongesMin);
        if(pasDeConge) {
            result=markerNoCongeLook;
        } else {
            if(cerclePlein) {
                result=fullMarkerLook
            }
        }
        return result;
    }
   
    
    // Affichage du Port : CircleMarker + Tooltip permanent (étiquette de nom - avec collision) + Tooltip sticky (données dispo)
    const markerRadius=computeRadius(source, annee)*PortViewUtils.magnifyingFactor;
    // console.log(data.name+": r="+markerRadius+" en "+data.point)

    return (<>
        <Circle
            center={data.point}
            pathOptions={computePathOptions(source, annee)}
            radius={markerRadius} 
        >
        {/*
             déporté dans PortNames : <Tooltip 
                permanent 
                className={"port-conges-tooltip conges"} 
                opacity={1} 
            >{data.name}</Tooltip> */}
        </Circle>
        {/*<StripedCircle centerLatLng={data.point} squareSide={markerRadius*2} 
            strokeColor={color_Stroke} strokeOpacity={opacity_Stroke} strokeWidth={width_Stroke} fillOpacity={opacity_full_marker} 
            orientation={30} 
            percentageAndColorCouples={[
                {percentage:10, color:"#66c2a5"}, 
                {percentage:30, color:"#fc8d62"}, 
                {percentage:60, color:"#8da0cb"}
            ]} 
        />*/}
        <PortHasAClerk data={data} />
        {/*  
            Pane vaut par défaut : 'overlayPane'
            Le tooltip suivant a besoin d'être au-dessus du reste : markers, vectors, ...
            => on l'ajoute à la couche juste au-dessus : 'shadowPane'
            Voir la liste des couches avec leurs Z-index : https://leafletjs.com/reference.html#map-overlaypane
        */}
        <Circle
            center={data.point}
            pathOptions={markerInvisibleLook}
            radius={markerRadius}
            pane="shadowPane"
        >
            <Tooltip sticky>
                <PortCongesDataView portData={data} source={source} annee={annee} />
            </Tooltip>
        </Circle>
    </>)
}