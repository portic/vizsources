// PortNames.jsx
/**
 * Affichage de tous les noms des ports, toujours classés par ordre de CONGES décroissants (source = "all", annee = "all")
 * Ajoute un nom de classe à chaque tooltip (étiquette de nom de port), pour permettre le fonctionnement de DetectOverlappingTooltips (détection de collision)
 */

import React from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { Marker, Tooltip } from 'react-leaflet'
import '../../../css/tooltips-custom.css'

export const PortNames = ({layerData, classNameForPortNames}) => {

    // Rendu du composant :
    return (<>
        { layerData.orderedDataListForPortNames && layerData.orderedDataListForPortNames.map(objPortData => {
            return (
                <Marker 
                    position={objPortData.point} 
                    icon={L.divIcon({className: "hidden-div-icon"})} 
                    key={objPortData.uhgs_id} 
                >
                    <Tooltip 
                        permanent 
                        className={"port-conges-tooltip "+classNameForPortNames} 
                        opacity={1} 
                    >{objPortData.name}</Tooltip>
                </Marker>
            )}
        )}
    </>);
}