// PortCongesDataView.jsx
/**
 * Visualisation des données : affichage des congés disponibles pour un port
 * (affiché au survol d'un port à la souris, sur la couche des congés)
 * 
 * translate prefix : portDataView
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, Table } from 'react-bootstrap';
import React from 'react';
import { Trans, useTranslation } from 'react-i18next';

export const PortCongesDataView = ({portData: data, source, annee}) => {
    const { t } = useTranslation();

    // Mise en forme du texte
    const colNoHorizMargin="px-0";
    const contentRow="pb-1 pt-2";
    const tableNoMargin="my-0";

    // Rendu du composant :
    // data.dump()
    const sources=data.availableSources;    // Liste des différentes sources pour ce port (objets Source)
    const annees=data.availableYears    // Liste des années pour lesquelles on a des données pour ce port
    const [tonnage, tonnageDeducedPercent]=data.computeTonnage(source, annee);
    const status=data.status;
    const substate=data.substate;
    const admiralty=data.admiralty;
    const province=data.province;
    const shiparea=data.shiparea;

    return (
        <Container className={"pb-1 px-4"} >
            <Row className={contentRow} >
                <Col className={colNoHorizMargin} >
                    <div className={"text-center"} ><strong>{data.name} : {data.lastNbCongesComputed}</strong></div>
                </Col>
            </Row>
            <Row className={contentRow} >
                { ((sources.length == 0) || (annees.length == 0)) &&
                    <Col className={colNoHorizMargin} >
                        <Trans i18nKey="portDataView.no_data">No data</Trans>
                    </Col>
                }
                { sources.length != 0 && annees.length != 0 &&
                    <Col className={colNoHorizMargin} >
                        <Table striped bordered hover size="sm" className={tableNoMargin} >
                            <thead>
                                <tr>
                                    <th className={"text-center"} ><Trans i18nKey="portDataView.source">Source</Trans></th>
                                    {
                                        annees.map(annee => (
                                            <th key={annee} className={"text-center"} >{annee}</th>
                                        ))
                                    }
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    sources.map(source =>(
                                        <tr key={source.abbrev} >
                                            <td>{source.displayName}</td>
                                            {
                                                annees.map(annee => (
                                                    <td key={source.abbrev+"_"+annee}>
                                                        <div className={"text-center"} >{data.getCongesFor(source.trueName, parseInt(annee))}</div>
                                                    </td>
                                                ))
                                            }
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </Table>
                        <div className={"text-center"} ><strong>{t('portDataView.entered_data', "data entered")}</strong> / {t('portDataView.reports_data', "data from reports")}</div>
                    </Col>
                }
            </Row>
            { sources.length != 0 && 
                <Row className={contentRow} >
                    <Col className={colNoHorizMargin} >
                        {t('portDataView.tonnage', "tonnage")} :  {tonnage === null ? "-" : tonnage+" "+t('portDataView.tonnage_deduced_percent', "(deduced part {{ deducedValue, number(style: 'percent'; maximumSignificantDigits: 2) }})", {deducedValue: tonnageDeducedPercent/100.0})}
                    </Col>
                </Row>
            }
            <Row className={contentRow} >
                <Col className={colNoHorizMargin} >
                    {t('portDataView.status', "status")} :  {status === null ? "-" : status}
                </Col>
            </Row>
            <Row className={contentRow} >
                <Col className={colNoHorizMargin} >
                    {t('portDataView.admiralty', "admiralty")} :  {admiralty === null ? "-" : admiralty}
                </Col>
            </Row>
            <Row className={contentRow} >
                <Col className={colNoHorizMargin} >
                    {t('portDataView.province', "province")} :  {province === null ? "-" : province}
                </Col>
            </Row>
        </Container>
    )

}
