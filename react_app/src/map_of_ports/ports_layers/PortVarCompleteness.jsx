// PortVarCompleteness.jsx
/**
 * Pour un port donné, affiche la complétude d'une variable :
 * - cercle d'aire proportionnelle au nombre de pointcalls renseignés pour cette variable
 * - couleur proportionnelle au pourcentage (nombre de renseignés / total * 100)
 * - couleurs par pas de 20% ( vert clair <= 20%, jaune 21 à 40%, orange 41 à 60%, rouge 61 à 80%, violet 81 à 100%)
 */

 import 'bootstrap/dist/css/bootstrap.min.css';
 import { Container, Row, Col, Table } from 'react-bootstrap';
import React from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { Trans, useTranslation } from 'react-i18next';
import { Tooltip, Circle } from 'react-leaflet'
import '../../../css/tooltips-custom.css'
import { PortViewUtils } from './PortViewUtils'

export const PortVarCompleteness = ({data, variable, source, annee}) => {
    const { t } = useTranslation();

    // Mise en forme :
    const colNoHorizMargin="px-0";
    const contentRow="pb-1 pt-2";
    const contentRowSeparated="pb-1 pt-3";
    const tableNoMargin="my-0";

    // Paramètres pour le marqueur :
    const radiusMin=3;
    const variableMin=3;

    // Calcul du rayon du cercle : aire du cercle proportionnelle au nombre TOTAL de pointcalls dans le port
    const computeRadius = (variable, source, annee) => {
        var result=2;   // Valeur par défaut : quand il n'y a pas de donnée pour la combinaison (variable, source, annee)
        
        const [total, variableValue, deducedValue]=data.computeVariable(variable, source, annee);
        if(total >= variableMin) {  // pour les trop petites valeurs, renvoyer la valeur par défaut
            // Calculer le rayon du marqueur cercle, pour que l'aire du marqueur soit proportionnelle au nb. total de pointcalls :
            result=PortViewUtils.radiusForCircleMarker(total, variableMin, radiusMin);
        }
        return result;
    }


    // Mise en forme du CircleMarker :
    // Palette séquentielle de rouges Multi-hue du site : https://colorbrewer2.org/#type=sequential&scheme=Purples&n=5
    const color_zeroValue='black'   // noir si pas de donnée du tout (quelque soient : variable, source, annee)
    const color_Value_20='#fef0d9'    // jusqu'à 20% : le rouge le plus clair
    const color_Value_40='#fdcc8a'    // de 20% à 40% : 
    const color_Value_60='#fc8d59'    // de 40% à 60% : 
    const color_Value_80='#e34a33'    // de 60% à 80% : 
    const color_Value_100='#b30000'    // de 80% à 100% : le rouge le plus foncé
    const color_Stroke='#212529'   // contour gray-900 (presque noir)
    // Cercle plein 20%
    const marker20Look={ stroke: true, color: color_Stroke, opacity: 0.9, weight: 1, fill: true, fillColor: color_Value_20, fillOpacity: 0.8 }    
    // Cercle plein 40%
    const marker40Look={...marker20Look, fillColor: color_Value_40}
    // Cercle plein 60%
    const marker60Look={...marker20Look, fillColor: color_Value_60}
    // Cercle plein 80%
    const marker80Look={...marker20Look, fillColor: color_Value_80}
    // Cercle plein 100%
    const marker100Look={...marker20Look, fillColor: color_Value_100}
    // Pas de donnée (petit point noir)
    const markerNoValueLook={color: color_zeroValue, stroke: false, fill: true, fillOpacity: 1}
    // Cercle invisible pour tooltip d'infos
    const markerInvisibleLook={ stroke: true, opacity: 0, weight: 1, fill: true, fillOpacity: 0}

    // Formatage du marqueur Cercle : couleur en fonction du POURCENTAGE de complétude de la variable (par plages de 20%)
    const computePathOptions = (variable, source, annee) => {
        var result=markerNoValueLook;  // Par défaut, cercle vide

        const [total, variableValue, deducedValue]=data.computeVariable(variable, source, annee);
        if(total < variableMin) {   // Pas de donnée
            result=markerNoValueLook;
        } else {    // Aspect en fonction de la valeur en % :
            if(variableValue <= 20) {
                result=marker20Look;
            }
            if((variableValue > 20) && (variableValue <= 40)) {
                result=marker40Look;
            }
            if((variableValue > 40) && (variableValue <= 60)) {
                result=marker60Look;
            }
            if((variableValue > 60) && (variableValue <= 80)) {
                result=marker80Look;
            }
            if((variableValue > 80) && (variableValue <= 100)) {
                result=marker100Look;
            }
        }
        return result;
    }

    // Prépare le texte à afficher au survol du port à la souris
    const getTooltipTxt = (variable) => {
        const [total, variableValue, deducedValue]=data.computeVariable(variable, source, annee);
        var valeurTxt=""
        var result=data.name+" : ";

        if(total == 0) {
            result+=t('portVarCompleteness.no_data', "No data");
        } else {
            // valeurs :            
            if(variable == "products") {    // pas de part déduite
                valeurTxt=t('portVarCompleteness.complete_x_percent_no_deduced', "({{ variableValue, number(style: 'percent'; maximumSignificantDigits: 2) }} complete)", {variableValue: variableValue/100.0});
            } else {    // avec part déduite
                valeurTxt=t('portVarCompleteness.complete_x_percent', "({{ variableValue, number(style: 'percent'; maximumSignificantDigits: 2) }} complete, deduced part {{ deducedValue, number(style: 'percent'; maximumSignificantDigits: 2) }})", {variableValue: variableValue/100.0, deducedValue: deducedValue/100.0});
            }
            // result :
            result+=total+" "+valeurTxt;
            // console.log(data.name+" : complet à "+variableValue+" %, part déduite "+deducedValue+" %")
        }
        return result;
    }

    // Formatte : tonnage connu / part déduite en %
    const getGoodSumTonnageInfo = (sourceAbbrev, annee) => {
        const [tonnage, tonnageDeducedPercent]=data.computeTonnage(sourceAbbrev, annee);
        return <>{ tonnage === null ? "-" : tonnage }{ tonnage === null ? "" : " / "+t("{{ val, number(style: 'percent'; maximumSignificantDigits: 2) }}", {val: tonnageDeducedPercent/100.0}) }</>
    }
   
    
    // Affichage du Port : CircleMarker + Tooltip permanent (étiquette de nom - avec collision) + Tooltip sticky (données dispo)

    const markerRadius=computeRadius(variable, source, annee)*PortViewUtils.magnifyingFactor;
    // console.log(data.name+": r="+markerRadius+" en "+data.point)
    const sources=data.availableSources;    // Liste des différentes sources pour ce port (objets Source)
    const annees=data.availableYears    // Liste des années pour lesquelles on a des données pour ce port
    const hasNoData=((sources.length == 0) || (annees.length == 0)) // Pas du tout de données pour ce port (point noir)

    return (<>
        <Circle
            center={data.point}
            pathOptions={computePathOptions(variable, source, annee)}
            radius={markerRadius} 
        >
            {/* déporté dans PortNames : <Tooltip 
                permanent 
                className={"port-conges-tooltip variable"} 
                opacity={1} 
            >{data.name}</Tooltip> */}
        </Circle>
        {/*  
            Pane vaut par défaut : 'overlayPane'
            Le tooltip suivant a besoin d'être au-dessus du reste : markers, vectors, ...
            => on l'ajoute à la couche juste au-dessus : 'shadowPane'
            Voir la liste des couches avec leurs Z-index : https://leafletjs.com/reference.html#map-overlaypane
        */}
        <Circle
            center={data.point}
            pathOptions={markerInvisibleLook}
            radius={markerRadius}
            pane="shadowPane"
        >
            <Tooltip sticky>
                { (hasNoData || (variable != "tonnage")) && getTooltipTxt(variable) }
                { !hasNoData && (variable == "tonnage") && <Container className={"pb-1 px-4"} >
                    <Row className={contentRow} >
                        <Col className={colNoHorizMargin} >
                            {getTooltipTxt(variable)}
                        </Col>
                    </Row>
                    <Row className={contentRowSeparated} >
                        {t('portDataView.tonnage_title', "Tonnage")} :
                    </Row>
                    <Row className={contentRow} >
                        <Table striped bordered hover size="sm" className={tableNoMargin} >
                            <thead>
                                <tr>
                                    <th className={"text-center"} ><Trans i18nKey="portDataView.source">Source</Trans></th>
                                    {
                                        annees.map(annee => (
                                            <th key={annee} className={"text-center"} >{annee}</th>
                                        ))
                                    }
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    sources.map(source =>(
                                        <tr key={source.abbrev} >
                                            <td>{source.displayName}</td>
                                            {
                                                annees.map(annee => (
                                                    <td key={source.abbrev+"_"+annee}>
                                                        <div className={"text-center"} >{getGoodSumTonnageInfo(source.trueName, parseInt(annee))}</div>
                                                    </td>
                                                ))
                                            }
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </Table>
                        <div className={"text-center"} >{t('portDataView.tonnage', "known tonnage (in barrels)")} / {t('portDataView.deduced', "deduced part")}</div>
                    </Row>
                </Container>}
            </Tooltip>
        </Circle>
    </>)
}