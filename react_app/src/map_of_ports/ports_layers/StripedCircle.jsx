// StripedCircle.jsx
/**
 * Cercle avec différentes "Tranches" de couleurs, pour afficher des proportions d'un total.
 * Symbole en svg, avec code de test : https://observablehq.com/@jgaffuri/texture-for-dorling-categorical-cartogram?collection=@jgaffuri/striped-circles 
 * => cercle remplis d'une texture : la texture comporte les tranches, et le cercle limite la partie visible de chaque tranche.
 * La limitation des tranches due au cercle impose de corriger les tranches, voir l'article : https://observablehq.com/@jgaffuri/striped-circle 
 * On peut faire varier l'angle des hachures pour représenter une variable supplémentaire.
 * 
 * percentageColorCouples : tableau (Array) de 3 couples { percentage: pourcentage, color: "#66c2a5"} décrivant les 3 tranches du cercle
 * percentageAndColorCouples={[
                {percentage:10, color:"#66c2a5"}, 
                {percentage:30, color:"#fc8d62"}, 
                {percentage:60, color:"#8da0cb"}
            ]}
 * orientation : angle [-180, 180], passer 0 pour des tranches verticales
 * centerLatLng : où afficher le marqueur sur la carte. Passer un L.latLng(50.5, 30.5)
 * squareSide : côté d'un carré contenant le cercle (le composant rajoute quelques mètres en plus de part et d'autre pour le lissage)
 * strokeColor, strokeOpacity, strokeWidth, fillOpacity : stroke = mise en forme du contour du cercle, fillOpacity = opacité des tranches de couleur à l'intérieur du cercle
 */

import React from 'react';
import { SVGOverlay } from 'react-leaflet'
import { PortViewUtils } from './PortViewUtils'

export const StripedCircle = ({percentageAndColorCouples, orientation, centerLatLng, squareSide, strokeColor, strokeOpacity, strokeWidth, fillOpacity}) => {

    // Construit la liste des rectangles composant le pattern (motif de remplissage) du cercle
    const getRectsOfPattern = (percentageAndColorCouples) => {
        var decalX=0
        var result=Array()
        var key=0

        for (const couple of percentageAndColorCouples) 
        {
            const aRect=<rect x={decalX} y="0" width={couple.percentage} height="100" style={{"fill": couple.color}} key={key} />
            result.push(aRect)  // Ajoute le rectangle au tableau du résultat
            decalX+=couple.percentage
            key++
        }
        return result
    }

    // Rectangle géographique permettant de fixer le svg sur la carte
    const bounds = PortViewUtils.squareBounds(centerLatLng, squareSide+55)

    return (
        <SVGOverlay attributes={{ viewBox: "0 0 105 105" }} bounds={bounds} pane={'overlayPane'} >
            <defs>
                <pattern id="pattern1" x="0" y="0" width="1" height="1" patternUnits="objectBoundingBox" patternTransform={`rotate(${orientation}, 52.5, 52.5)`}>
                    {getRectsOfPattern(percentageAndColorCouples)}
                </pattern>
            </defs>
            {/*<rect x="0" y="0" width="100%" height="100%" fill="blue" />*/}
            <circle cx="52.5" cy="52.5" r="50" style={{ stroke: "none", fill: "url(#pattern1)", fillOpacity: fillOpacity }} />
            <circle cx="52.5" cy="52.5" r="50" style={{ stroke: strokeColor, strokeOpacity: strokeOpacity, strokeWidth: strokeWidth, fill: "none"}} />
        </SVGOverlay>
    )
}
