// VizSources.jsx
/**
 * Application React de visualisation des sources :
 * - chargement des données de l'API porticapi (à l'URL : apiurl)
 * - affichage des composants d'interface utilisateur, et d'affichage de carte
 * 
 * Bootstrap 5 : https://getbootstrap.com/docs/5.0/getting-started/webpack/ et https://getbootstrap.com/docs/5.0/getting-started/contents/ 
 * React-Bootstrap 2 : https://react-bootstrap.github.io/getting-started/introduction/
 * Axios : https://axios-http.com/
 * 
 * translate prefix : vizSources
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import axios from 'axios';
import { UrlParamsParser } from './UrlParamsParser'
import { MsgLoad } from './user_interface/MsgLoad'
import { MapOfFrenchPorts } from './map_of_ports/MapOfFrenchPorts'
import { RowNavbarViz } from './user_interface/RowNavbarViz'
import { RowControlsViz } from './user_interface/RowControlsViz'
import '../css/vizsources.css'


export const VizSources = ({apiurl}) => {   // déstructure les props pour avoir les variables directement dans le composant fonctionnel
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    // Valeurs initiales pour le state : Parser les paramètres contenus dans l'URL de l'application
    // (ex.: http://127.0.0.1:5000/index.html?source=g5&center=[46.86,2.90] )
    // les valeurs initiales par défaut des paramètres (et le parseur) sont dans : UrlParamsParser.js
    const urlParamsParser=new UrlParamsParser(window.location.search)   // ex.: window.location.search = ?source=g5&center=[46.86,2.90] 
    // console.log(urlParamsParser.listParams) // debug : afficher les paramètres, reçus ou par défaut
        
    // Dans le state :
    // * apiData : les données au format JSON, ou null si pas chargées
    const [apiData, setApiData] = useState(null);
    // * loadStatus : pour connaître l'état du chargement des données / le message d'erreur
    const [loadStatus, setLoadStatus] = useState({
        apiMessage: "", // message reçu pendant le chargement des données si erreur
        apiInfo: null,    // information complémentaire (debug)
        apiInfoWrap: false, // true : Ne pas afficher d'ascenseur horizontal, mais passer à la ligne (pour erreurs autres que 500)
        loading: true,  // true pendant le chargement, false quand c'est fini
        error: false // true si une erreur s'est produite pendant le chargement, false sinon
    });
    // * variable : la variable représentée sur la carte (conges, tonnage, ...)
    const [variable, setVariable] = useState(urlParamsParser.listParams.variable)
    // * source : la source sélectionnée pour la visualisation (g5, marseille, cabotage, expeditions)
    const [source, setSource] = useState(urlParamsParser.listParams.source)
    // * annee : l'année pour laquelle on regarde les données de la source (1787, 1789)
    const [annee, setAnnee] = useState(urlParamsParser.listParams.annee)
    // * print : mode impression (true) => ne pas afficher le zoom, ...
    const [print, setPrint] = useState(urlParamsParser.listParams.print)
    // * printValue : to re-generate the map when print change
    const [printValue, setPrintValue] = useState(print)
    // * center : where the map's center is located (lat, lng)
    const [center, setCenter] = useState(urlParamsParser.listParams.center)
    // * zoom : zoom level of the map
    const [zoom, setZoom] = useState(urlParamsParser.listParams.zoom)
    // * world : map background
    const [world, setWorld] = useState(urlParamsParser.listParams.world)
    

    // Requêtes à l'API :
    // with Leaflet, default is : (Map's display CRS = EPSG:3857) / Map's data CRS = EPSG:4326
    // - données 'nombre de congés' disponibles (saisies=inputDone ou connues=cr) pour chaque port :
    // const requetePorticapi = `${apiurl}/sources/?srid=4326&lang=${lang}&params=ogc_fid,toponym,source_suite,pointcall_year,nb_conges_inputdone,nb_conges_cr,nb_conges_sante,nb_petitcabotage,nb_longcours_marseille,total,good_sum_tonnage,point,status,has_a_clerk,admiralty,province&order=toponym,source_suite,pointcall_year`
    const requetePorticapi = `${apiurl}/sources/?srid=4326&lang=${lang}&order=toponym,source_suite,pointcall_year`

    // Gérer une erreur renvoyée lors de la communication avec l'API
    const gererErreurApi = (error) => {
        // Erreur : dans le navigateur (error) ou sur le serveur (error.response)
        // error.response avec Flask/jsonify() : {data (JSON - simple chaine), status (404), statusText (NOT FOUND), headers, config, request}
        // console.log(error.response)
        var apiMsg = "";
        var apiInfo = null;
        var apiInfoWrap=false;
        if((typeof error.response) == 'undefined') {    // Pas de réponse du serveur
            apiMsg = error.toString();
            // apiInfo = error.response.data;
            apiInfo=""
            apiInfoWrap = false;
        } else {    // On a une réponse du serveur
            // Valeurs par défaut :
            apiMsg = <>{error.toString()} - {error.response.statusText}</>;
            apiInfo = error.response.data;
            apiInfoWrap=true;   // Passer à la ligne, pas d'ascenseur horizontal
            // Personnalisation des infos de déboguage :
            switch(error.response.status) {
                case 404 :  // not found
                    apiInfo = error.response.request.responseURL+'\n\n'+error.response.data;  // Ajouter l'URL demandée
                    break;
                case 405 :  // method not allowed
                    // Ajouter la méthode utilisée :
                    apiMsg = <>{error.toString()} - {error.response.config.method.toString().toUpperCase()} {error.response.statusText}</>;
                    break;
                case 500 :  // internal server error
                    // ascenseur horizontal (pas de retour à la ligne forcé pour préserver la lisibilité des messages de Pandas SQL) :
                    apiInfoWrap=false;  
                    break;
                default :
                            
            }
        }
        // Mémoriser le message d'erreur et le status du chargement :
        setLoadStatus({ apiMessage: apiMsg, apiInfo: apiInfo, apiInfoWrap: apiInfoWrap, loading: false, error: true });
    }

    // Charger les données de l'API porticapi
    const chargerDonnees = () => {
        // mettre le loadStatus à : chargement en cours
        setLoadStatus({ apiMessage: '', apiInfo: null, apiInfoWrap: false, loading: true, error: false })
        // Interroger l'API :
        axios.get(requetePorticapi)
            .then(res => {  // Succès :
                // stocker les données reçues :
                const newApiData={
                    portsData: res.data, 
                }
                setApiData(newApiData)
                // mettre à jour le status du chargement :
                setLoadStatus({ ...loadStatus, loading: false, error: false })  // chargement terminé sans erreur
            })
            .catch(error => { 
                gererErreurApi(error) // Gestion d'erreur avec l'API
            });
    }

    // Charger les données de l'API
    useEffect(() => {
        // Requête à l'API + stockage dans le State
        chargerDonnees();   
    }, [lang]); // Effet exécuté initialement + ré-exécuté seulement si lang change (rechargement des traductions des noms des ports : toponym, ...)

    // Faire sur changement de 'print' : 
    useEffect(() =>  {
        // MAJ de printValue, pour mettre à jour la carte à afficher :
        setPrintValue(print);
    }, [print]);


    // Rendu du composant :

    // Chargement en cours
    if(loadStatus.loading) {
        return (<MsgLoad erreur={false} message={t('vizSources.load_in_progress', 'Data loading ...')} />)
    }

    // Erreur de chargement
    if(loadStatus.error) {
        // Dev :
        return (<MsgLoad erreur={true} message={<>{t('vizSources.load_error', 'Error while loading data : ')}<br />{loadStatus.apiMessage}</>} info={loadStatus.apiInfo} infoWrap={loadStatus.apiInfoWrap} />)
        // Prod :
        // return (<MsgLoad erreur={true} message={<>{t('vizSources.load_error', 'Error while loading data : ')}<br />{loadStatus.apiMessage}</>} />)
    }

    // Données chargées sans erreur
    if(!loadStatus.loading && !loadStatus.error) {
        // console.log(apiData)    // voir ce qui a été chargé

        // Composants : barre de navigation (menu), contrôles d'interface utilisateur, et carte.
        // Taille : taille de la carte fixée sur le conteneur de la carte (XXvh), dans MapOfFrenchPorts.jsx
        return (
            <Container>
                <RowNavbarViz bgColorClass={"bg-light"} txtClass={"txtNavBar"} />
                <RowControlsViz 
                    variable={variable} changeVariable={setVariable} 
                    source={source} changeSource={setSource} 
                    annee={annee} changeAnnee={setAnnee} 
                    print={print} changePrint={setPrint} 
                    center={center} zoom={zoom} world={world} 
                />
                { (print == printValue) && <Row>
                    <Col className={"my-2 mx-2 px-0"} >
                        <MapOfFrenchPorts portsDataFromAPI={apiData.portsData} 
                            variable={variable} 
                            source={source} 
                            annee={annee} 
                            print={printValue} 
                            center={center} changeCenter={setCenter} 
                            zoom={zoom} changeZoom={setZoom} 
                            world={world} changeWorld={setWorld} 
                        />
                    </Col>
                </Row>}
            </Container>
        )
    }
}