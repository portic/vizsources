// i18nextInit.js
/**
 * Initialisation du système de localisation (internationalisation) : i18next et react-i18next
 * AVANT le démarrage de l'application React => à inclure dans index.js
 * 
 * Initialise le plugin pour détecter la langue du navigateur, charge la langue détectée,
 * et connecte react-i18next à i18next.
 * 
 * Articles indiquant la procédure d'internationalisation :
 * https://medium.com/@danduan/translating-react-apps-using-i18next-d2f78bc87314
 * https://dev.to/adrai/how-to-properly-internationalize-a-react-application-using-i18next-3hdb
 * 
 * i18next : https://www.i18next.com/
 * React-i18next : https://react.i18next.com/
 */

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

// charger les traductions :
import en from './locales/en.json';
import fr from './locales/fr.json';

// Liste des langues, pour le sélecteur de langue (choix de la langue par l'utilisateur) :
export const langsAvailable = ['en', 'fr'];

// options du plugin de détection :
const langDetectorOptions = {
    // order and from where user language should be detected
    order: ['localStorage', 'navigator'],
  
    // keys or params to lookup language from
    lookupLocalStorage: 'locale',
  
    // cache user language on
    caches: ['localStorage'],
    excludeCacheFor: ['cimode'], // languages to not persist (cookie, localStorage)
  
    // only detect languages that are in the whitelist
    checkWhitelist: true,
};

// Initialisation :
i18n
// detect user language
// learn more: https://github.com/i18next/i18next-browser-languageDetector
    .use(LanguageDetector)
// pass the i18n instance to react-i18next.
    .use(initReactI18next)
// init i18next :
// for all options read: https://www.i18next.com/overview/configuration-options
    .init({
// use the translations :
        resources: {
            en: { // translation is the default namespace
                translation: en
            },
            fr: {
                translation: fr
            }
        },
        fallbackLng: 'en',
        debug: false,
        whitelist: langsAvailable, // available languages for browser dector to pick from
        detection: langDetectorOptions,
        interpolation: {
            escapeValue: false, // not needed for react as it escapes by default
        }
    });

export default i18n;