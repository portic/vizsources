// UrlParamsParser.js
/**
 * Classe parseur des paramètres passés dans l'URL de l'application VizSources.
 * 
 * Permet d'obtenir la liste des valeurs des paramètres de l'application : paramsList
 * - paramètres reconnus : source, année, variable, centre de la carte, niveau de zoom, print
 * - liste des valeurs autorisées pour chaque paramètre
 * - valeurs par défaut à utiliser (si paramètre manquant ou valeur erronée)
 * - signale les valeurs erronées dans la console
 * 
 * Documentation avec JSDoc : https://en.wikipedia.org/wiki/JSDoc
 * 
 * translate prefix : vizSources
 */

import { t } from "i18next";

export class UrlParamsParser {
    // Résultat : Liste des paramètres décodés (ou sinon par défaut) :
    listParams={}
    // (stocké dans l'instance de la classe)

    // Valeurs autorisées :
    static sourcesOk=["g5", "marseille", "cabotage", "expeditions"]
    static anneesOk=["1749", "1759", "1769", "1779", "1787", "1789", "1799"]
    static variablesOk=["conges", "tonnage", "homeport", "products", "birthplace", "citizenship"]
    static booleanOk=["true", "false"]  // pour 'print'
    static worldOk=["shorelines", "1789"]   // Fonds de carte

    // Valeurs par défaut :
    static defaultListParams={
        "source": "g5",
        "annee": "1787",
        "variable": "conges",
        "center": {lat: 46.860191, lng: 2.900391},  // format txt attendu : center=[lat,lng] avec -90 <= lat <= 90 et -180 <= lng <= 180
        "zoom": 6,
        "print": false, 
        "world": "shorelines"
    }

    // Table de correspondance permettant de retrouver un fond de carte (world) par son nom d'affichage (selon la langue sélectionnée)
    static worldDisplayNameKeys=[   
        { layerDisplayNameKey: 'worldOverlay.overlay_name_shorelines', world: "shorelines" }, 
        { layerDisplayNameKey: 'worldOverlay.overlay_name_world1789', world: "1789" }, 
    ]


    /**
     * Constructeur qui parse (isole / valide / stocke) les paramètres reçus par l'URL de l'application
     * ex. d'URL : http://127.0.0.1:5000/index.html?source=g5&center=[46.86,2.90]
     * partie contenant les paramètres : ?source=g5&center=[46.86,2.90]
     * pour obtenir les paramètres de l'URL courante, utiliser : window.location.search
     * @see https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
     * @param {string} parametersPortionOfUrl La portion de l'URL de l'application qui contient les paramètres (?param=value&otherParam=otherValue) : window.location.search
     */
    constructor(parametersPortionOfUrl) {
        this.parseParameters(parametersPortionOfUrl)    // isole / valide / stocke les paramètres dans la chaîne passée au constructeur
    }

    /**
     * Emet un message d'erreur. 
     * Signale une erreur dans l'URL. 
     * Si on veut modifier la façon de signaler une erreur dans l'URL de l'application, c'est ici qu'il faut le faire. 
     * @param {string} errorMsg message d'erreur
     */
    signalUrlError(errorMsg) {
        // Signaler l'erreur dans la console :
        console.error(errorMsg)
    }

    /**
     * Signaler un paramètre incorrect passé dans l'URL
     * @param {string} paramName le paramètre pour lequel il y a un problème
     * @param {boolean} isUnknownParam true si paramètre inconnu. false si problème avec la valeur du paramètre
     * @param {string} value la valeur reçue pour ce paramètre
     * @param {Array} tabKnownValues liste (tableau) des valeurs autorisées (attendues) pour ce paramètre
     */
    signalIncorrectUrlParam(paramName, isUnknownParam, value, tabKnownValues) {
        var errorMsg=""

        if(isUnknownParam) {    // Nom de paramètre inconnu
            errorMsg=t('vizSources.unknown_param_error', "Error in URL : '{{ paramName }}' unknown parameter", {paramName: paramName})
        } else {    // Paramètre connu, mais valeur incorrecte
            errorMsg=t('vizSources.incorrect_value_error', "Error in URL : bad value '{{ value }}' for parameter '{{ paramName }}'. Correct values are : {{ listOfValues }}", {
                paramName: paramName,
                value: value,
                listOfValues: tabKnownValues.toString()
            })
        }
        this.signalUrlError(errorMsg)
    }

    /**
     * Lecture des paramètres passés dans l'URL (pour remplir this.paramsList). 
     * Appelée par le constructeur. 
     * @param {string} parametersPortionOfUrl la portion de l'URL de l'application qui contient les paramètres (?param=value&param2=value2&...)
     */
    parseParameters(parametersPortionOfUrl) {
        this.listParams={}  // ré-initialisation
        var errorMsg=""
        
        // Copier d'abord les valeurs par défaut des paramètres de l'URL :
        this.listParams=Object.assign(this.listParams, UrlParamsParser.defaultListParams)

        // Lire les paramètres de l'URL de l'application :
        const queryParams = new URLSearchParams(parametersPortionOfUrl)
        // Parcourir la liste pour valider les paramètres attendus :
        for (const [param, value] of queryParams) {
            switch(param) {
                case "source" :
                    if (UrlParamsParser.sourcesOk.includes(value)) {    // valeur OK
                        this.listParams={...this.listParams, "source": value} // remplacement dans la liste des paramètres  
                    } else {    // valeur incorrecte
                        this.signalIncorrectUrlParam(param, false, value, UrlParamsParser.sourcesOk)
                    }
                    break;
                case "year" :
                    if (UrlParamsParser.anneesOk.includes(value)) {    // valeur OK
                        this.listParams={...this.listParams, "annee": value} // remplacement dans la liste des paramètres  
                    } else {    // valeur incorrecte
                        this.signalIncorrectUrlParam(param, false, value, UrlParamsParser.anneesOk)
                    }
                    break;
                case "variable" :
                    if (UrlParamsParser.variablesOk.includes(value)) {    // valeur OK
                        this.listParams={...this.listParams, "variable": value} // remplacement dans la liste des paramètres  
                    } else {    // valeur incorrecte
                        this.signalIncorrectUrlParam(param, false, value, UrlParamsParser.variablesOk)
                    }
                    break;
                case "center" :
                    // Valider la syntaxe :
                    // Regex : voir https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test et https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Regular_Expressions 
                    const regexValidCenter = /^\[[\d.-]+,[\d.-]+\]$/  // commence par '[', se termine par ']', contient deux nombres (chiffres et point) séparés par une virgule
                    const centerCorrect=regexValidCenter.test(value)
                    if (!centerCorrect) { // format incorrect
                        errorMsg=t('vizSources.incorrect_center_format_error', "Error in URL : bad format for parameter 'center'. Expected format for [lat,lng] : center=[46.860191,2.900391] with -90 <= lat <= 90 and -180 <= lng <= 180")
                        this.signalUrlError(errorMsg)
                    } else {    // Format correct
                        // Retrouver : latitude et longitude, dans la valeur passée
                        const regexDecodeCenter = /^\[(?<strLatitude>[\d.-]+),(?<strLongitude>[\d.-]+)\]$/    // groupe nommé à capturer : (?<nomGroupe>motif)
                        const centerFound=value.trim().match(regexDecodeCenter) // exécute la regex pour extraire les deux groupes nommés (strLatitude, strLongitude)
                        const latitude=Number(centerFound["groups"].strLatitude)    // conversion en flottant
                        const longitude=Number(centerFound["groups"].strLongitude)
                        const centerCoords={lat: latitude, lng: longitude}  // Objet destiné au constructeur de l'objet Leaflet : L.latLng(<Object> coords)
                        this.listParams={...this.listParams, "center": centerCoords} // remplacement dans la liste des paramètres
                    }
                    break;
                case "zoom" :
                    // Valider la syntaxe :
                    const regexValidZoom = /^[\d]+$/  // entier, sans virgule/point, sans rien d'autre avant ou après
                    const zoomCorrect=regexValidZoom.test(value)
                    if (!zoomCorrect) { // format incorrect
                        errorMsg=t('vizSources.incorrect_zoom_level_format_error', "Error in URL : bad format for parameter 'zoom'. Positive integer expected")
                        this.signalUrlError(errorMsg)
                    } else {    // Format correct
                        // Retrouver la valeur de zoom
                        const regexDecodeZoom = /^(?<strZoom>[\d]+)$/    // groupe nommé à capturer : (?<nomGroupe>motif)
                        const zoomFound=value.trim().match(regexDecodeZoom) // exécute la regex pour extraire le groupe nommé (strZoom)
                        const zoomLevel=Number(zoomFound["groups"].strZoom)    // conversion en entier
                        this.listParams={...this.listParams, "zoom": zoomLevel} // remplacement dans la liste des paramètres
                    }
                    break;
                case "print" :
                    const valueLowercase=value.trim().toLowerCase() // True -> true
                    if (UrlParamsParser.booleanOk.includes(valueLowercase)) {    // valeur OK
                        const printValue=(valueLowercase === 'true')
                        this.listParams={...this.listParams, "print": printValue} // remplacement dans la liste des paramètres  
                    } else {    // valeur incorrecte
                        this.signalIncorrectUrlParam(param, false, value, UrlParamsParser.booleanOk)
                    }
                    break;
                case "world" :
                    if (UrlParamsParser.worldOk.includes(value)) {    // valeur OK
                        this.listParams={...this.listParams, "world": value} // remplacement dans la liste des paramètres  
                    } else {    // valeur incorrecte
                        this.signalIncorrectUrlParam(param, false, value, UrlParamsParser.worldOk)
                    }
                    break;
                default:    // paramètre inconnu
                    this.signalIncorrectUrlParam(param, true, '', [])
                    break;
            }
        }
    }


};