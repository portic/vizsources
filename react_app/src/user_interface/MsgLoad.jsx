// MsgLoad.jsx
/**
 * Affichage d'un message texte au centre de l'écran lors du chargement :
 *      <MsgLoad erreur={false} message={"Chargement des données ..."} />
 * Affichage d'une erreur sur fond rouge (alert bootstrap):
 *      <MsgLoad erreur={true} message={"Erreur en faisant ..."} info={"Message d'erreur détaillé (facultatif - à enlever en production)"} infoWrap={true} />
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'react-bootstrap';
import React from 'react';

export const MsgLoad = ({message, erreur, info, infoWrap}) => {
    var infoMsg=((info == undefined) ? null : info);    // Si on ne passe la propriété 'info', alors infoMsg vaut : null
    var infoMsgWrap=((infoWrap == undefined) ? null : infoWrap);
    var infoMsgStyle={tabSize: "4"};    // style par défaut : tabulations=4 espaces, ascenseur horizontal (pas de passage à la ligne forcé)

    if(infoMsg) {  // Formatage du message d'information (message d'erreur) :
        // console.log(infoMsg)
        infoMsg=infoMsg.split('\\n').join('\n');
        infoMsg=infoMsg.split('\\t').join('\t');       
        if(infoMsgWrap) {  // En plus, forcer le passage à la ligne (pas d'ascenseur horizontal)
            infoMsgStyle= {...infoMsgStyle, whiteSpace: "pre-wrap"}
            // voir : https://developer.mozilla.org/fr/docs/Web/CSS/white-space
        }
    }

    return (
        <Container fluid  style={{height: '100vh'}}>
            <Row className="align-items-center justify-content-center h-100">
                <Col xs="10">
                    { erreur && <div className="alert alert-danger" role="alert">
                            <div className="text-center">{message}{ infoMsg && <> :<br /><br /></>}</div>
                            { infoMsg && <pre style={infoMsgStyle}>{infoMsg}</pre> }
                        </div>
                    }
                    { !erreur && <div className="alert alert-light text-center" role="alert">
                            {message}
                        </div>
                    }
                </Col>
            </Row>
        </Container>
    )
}