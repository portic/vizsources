// RowControlsViz.jsx
/**
 * Eléments d'interface utilisateur (formulaire), pour paramétrer la carte
 * 
 * Utilise les icônes FontAwesome Free v6 avec le composant React officiel (méthode SVG + JS) :
 * - installation : https://fontawesome.com/v6/docs/web/use-with/react/
 * - utilisation (méthode d'importation dynamique des icônes - nécessite un plugin Babel) : https://fontawesome.com/v6/docs/web/use-with/react/add-icons
 * - stylisation : https://fontawesome.com/v6/docs/web/use-with/react/style
 * - liste des icônes 'Free' : https://fontawesome.com/search?m=free
 * 
 * translate prefix : controlsViz
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import React from 'react';
import { Trans, useTranslation } from 'react-i18next';
import '../../css/vizsources.css'
import { Source } from '../map_of_ports/datamodel/Source'
import { SaveAsImageButton } from './SaveAsImageButton'
import { UrlButton } from './UrlButton'
import { UrlParamsParser } from '../UrlParamsParser'

// Interface utilisateur de paramétrage de la carte
export const RowControlsViz = ({variable, changeVariable, source, changeSource, annee, changeAnnee, print, changePrint, center, zoom, world}) => {  // center/zoom/world détectés / modifiés dans UpdateMapParameters.jsx
    const { t } = useTranslation();

    // Changement d'état d'un élément de formulaire
    const handleChange = (e) => {
        switch(e.target.name) {
            case "variable" : // Modification de la variable représentée
                changeVariable(e.target.value);
                break;
            case "source" : // Modification de la source
                changeSource(e.target.value);
                break;
            case "annee" : // Modification de l'année
                changeAnnee(e.target.value);
                break;
        }
    }

    // Affichage :

    const allSources=Source.getAllSources() // Tableau d'objets Source, dans l'ordre d'affichage souhaité
    const currentSource=allSources.find(element => element.abbrev == source) // recherche de l'abbrev. 'source' dans la table : allSources

    return (
        <Form as={Row} className={"py-0 px-2 d-flex justify-content-sm-start justify-content-md-center justify-content-xl-around"} >
            <Form.Group as={Col} xs={"auto"} className={"px-0"} controlId="source">
                <Container className={""} >
                    <Row>
                        <Col xs={"auto"} className={"ps-0"}>
                                <Form.Label>
                                    <Trans i18nKey="controlsViz.source">Source</Trans>
                                </Form.Label>
                        </Col>
                        <Col className={"ps-0"}>
                                {/*
                                <Form.Check type={"radio"} name={"source"} id={"source_g5"} label={t('controlsViz.src_g5', "G5 - Clearances (Tax)")} value={"g5"} checked={source == "g5"} onChange={handleChange} />
                                <Form.Check type={"radio"} name={"source"} id={"source_marseille"} label={t('controlsViz.src_sante_marseille', "Bill of Health in Marseille")} value={"marseille"} checked={source == "marseille"} onChange={handleChange} />
                                <Form.Check type={"radio"} name={"source"} id={"source_cabotage"} label={t('controlsViz.src_petit_cabotage', "Entrances (costal trade)")} value={"cabotage"} checked={source == "cabotage"} onChange={handleChange} />
                                <Form.Check type={"radio"} name={"source"} id={"source_expeditions"} label={t('controlsViz.src_expeditions', "Clearances (to America and Indian ocean)")} value={"expeditions"} checked={source == "expeditions"} onChange={handleChange} />
                                */}
                                {allSources.map(sourceObj => sourceObj.getRadioButton(source, handleChange))}
                        </Col>
                    </Row>
                </Container>
            </Form.Group>
            <Form.Group as={Col} xs={"auto"} className={"px-0"} controlId="annee">
                <Container className={""}>
                    <Row className={"align-items-center pb-2"} >
                        <Col className={"ps-0"}>
                                <Form.Label className={"mb-0"} >
                                    <Trans i18nKey="controlsViz.year">Year</Trans>
                                </Form.Label>
                        </Col>
                        <Col xs={"auto"} className={"ps-0"}>
                                {/* Boutons radio (remplacés par une liste déroulante ci-dessous) :
                                <Form.Check type={"radio"} name={"annee"} id={"annee_1787"} label={"1787"} value={"1787"} checked={annee == "1787"} onChange={handleChange} />
                                <Form.Check type={"radio"} name={"annee"} id={"annee_1789"} label={"1789"} value={"1789"} checked={annee == "1789"} onChange={handleChange} />
                                */}
                                <Form.Select as={Col} name={"annee"} id={"dropdown-select-annee-"+source}
                                    title={t('controlsViz.year_description', "Select an available year for the chosen source")} 
                                    aria-label={t('controlsViz.year_description', "Select an available year for the chosen source")} 
                                    value={annee} onChange={handleChange} 
                                    className={""}
                                >
                                    {/*
                                    <option key={"g5_1787"} value={"1787"} >{"1787"}</option>
                                    */}
                                    {UrlParamsParser.anneesOk.map(yearStr => currentSource.getSelectOptionForYear(yearStr))}
                                </Form.Select>
                        </Col>
                    </Row>
                </Container>
            </Form.Group>
            <Form.Group as={Col} xs={"auto"} className={"px-0"} controlId="variable">
                <Container className={""}>
                    <Row className={"align-items-center pb-2"} >
                        <Col className={"ps-0"}>
                                <Form.Label className={"mb-0"} >
                                    <Trans i18nKey="controlsViz.variable">Variable</Trans>
                                </Form.Label>
                        </Col>
                        <Col xs={"auto"} className={"px-0"}>
                                <Form.Select as={Col} name={"variable"} id={"dropdown-select-variable"}
                                    title={t('controlsViz.variable_description', "Select data to display on the map")} 
                                    aria-label={t('controlsViz.variable_description', "Select data to display on the map")} 
                                    value={variable} onChange={handleChange} 
                                    className={""}
                                >
                                    <option key={"conges"} value={"conges"} >{t('controlsViz.var_leave_fees', "Clearances (Tax)")}</option>
                                    <option key={"tonnage"} value={"tonnage"} >{t('controlsViz.var_tonnage', "Ship burthen")}</option>
                                    <option key={"homeport"} value={"homeport"} >{t('controlsViz.var_homeport', "Ship's homeport")}</option>
                                    <option key={"products"} value={"products"} >{t('controlsViz.var_products', "Cargo or scope of voyage")}</option>
                                    <option key={"birthplace"} value={"birthplace"} >{t('controlsViz.var_birthplace', "Captain local identification")}</option>
                                    <option key={"citizenship"} value={"citizenship"} >{t('controlsViz.var_citizenship', "Captain \"national\" identification")}</option>
                                </Form.Select>
                        </Col>
                    </Row>
                    <Row className={"align-items-center"}>
                        <Col xs={"auto"} className={"ps-0"}>
                            <Form.Check 
                                type={"checkbox"}
                                id={"checkbox-map-nocontrols"}
                                label={t('controlsViz.no_control', "Map with no control")} 
                                defaultChecked={print} 
                                onChange={() => {changePrint(!print)}}
                            />
                        </Col>
                        <SaveAsImageButton />
                        <UrlButton source={source} annee={annee} variable={variable} print={print} center={center} zoom={zoom} world={world} />
                    </Row>
                </Container>
            </Form.Group>              
        </Form>
            
    )
}