// SaveAsImageButton.jsx
/**
 * Bouton pour exporter la carte dans un fichier image : svg, png ou jpeg
 * Utilise le paquet npm : dom-to-image ( https://github.com/tsayen/dom-to-image )
 * Possibilité de ne pas enregistrer certains éléments de la carte : fonction de filtrage
 *      ex. de fonction de filtrage pour ignorer les éléments <i> : {filter: (node) => {return (node.tagName !== 'i');} }
 * Options :
 *      bgcolor - color for the background, any valid CSS color value.
 *      width - width to be applied to node before rendering.
 *      height - height to be applied to node before rendering.
 *      style - an object whose properties to be copied to node's style before rendering.
 *      quality - a Number between 0 and 1 indicating image quality (applicable to JPEG only),
                defaults to 1.0.
 *      imagePlaceholder - dataURL to use as a placeholder for failed images, default behaviour is to fail fast on images we can't fetch
 *      cacheBust - set to true to cache bust by appending the time to the request url
 * 
 * Utilise le composant React pour les icônes de Fontawesome v6 : <FontAwesomeIcon />
 * - installation : https://fontawesome.com/v6/docs/web/use-with/react/
 * - ajouter des icônes avec <FontAwesomeIcon /> : https://fontawesome.com/v6/docs/web/use-with/react/add-icons
 * - importation dynamique des seules icônes utilisées, grâce au paquet npm pour Babel : babel-plugin-macros
 * - stylisation : https://fontawesome.com/v6/docs/web/use-with/react/style
 * 
 * Les Refs avec useRef() : https://dev.to/carlosrafael22/using-refs-in-react-functional-components-part-1-useref-callback-ref-2j5i
 * 
 * translate prefix : saveAsImageButton 
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Button, Form } from 'react-bootstrap';
import React, { useState, useRef } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid, regular, brands } from '@fortawesome/fontawesome-svg-core/import.macro' // <-- import styles to be used
import domtoimage from 'dom-to-image';

export const SaveAsImageButton = ({}) => {
    const { t } = useTranslation();

    // Dans le state :
    // * selectFileFormat : true => la liste déroulante du format de fichier à sélectionner est visible
    const [selectFileFormat, setSelectFileFormat] = useState(false);

    // Refs :
    // * refSpinner : Référence à l'icône spinner à masquer / montrer :
    const refSpinner = useRef(null); // refSpinner => { current: null }
    // * refSelectFormat : Référence à la liste déroulante de choix de format d'image (pour réinitialisation) :
    const refSelectFormat = useRef(null);

    // Get a SVG dataURL for an element of the document
    const saveSVG = (domElementToSave, width, height) => {
        // get styles from all required stylesheets
        // http://www.coffeegnome.net/converting-svg-to-png-with-canvg/
        var style = "\n";
        // var requiredSheets = ['leaflet.css', 'world-background.css']; // list of required CSS
        for (var i=0; i<document.styleSheets.length; i++) {
            var sheet = document.styleSheets[i];
            var rules = sheet.cssRules;
            if (rules) {
                for (var j=0; j<rules.length; j++) {
                    style += (rules[j].cssText + '\n');
                }
            }
        }
        
        // prepend style to each svg element
        var mapElt=domElementToSave.cloneNode(true)
        var svgElts=mapElt.getElementsByTagName("svg")
        for (var i = 0; i < svgElts.length; i++) {
            var svg=svgElts[i]
            var defs=document.createElement("defs")
            svg.insertBefore(defs, svg.firstChild)
            var styleElt=document.createElement("style")
            styleElt.setAttribute('type','text/css')
            styleElt.innerHTML = style
            defs.append(styleElt)
        }
        
        // generate dataURL
        const serializer = new XMLSerializer()
        const svgStr = serializer.serializeToString(mapElt)
        const dataURL = 'data:image/svg+xml;base64,'+window.btoa(unescape(encodeURIComponent(svgStr)));
        return dataURL
    }

    // Enregistre l'image sous un nom prédéfini, avec l'extension de fichier déterminée par les données (dataUrl)
    // ajoute dans le document un lien pointant vers l'image produite par dom-to-image (dataUrl), et clique automatiquement sur ce lien (pour l'enregistrer)
    const saveImage=(dataUrl) => {
        var link = document.createElement('a')
        link.download = 'vizsources-map'
        link.href = dataUrl
        // Test handler d'erreur :
        // throw new Error("Invalid File System Error")
        link.click()
        // Masquer la liste de sélection, et remettre le bouton à l'état initial :
        setSelectFileFormat(false)
    }

    // Signale une erreur survenue lors de l'enregistrement de l'image de la carte
    const onSaveImageError=(error) => {
        var errorMsg=""

        // Masquer le spinner :
        refSpinner.current.style.visibility='hidden'
        // Remettre la liste de sélection de format d'image à -- l'invite de choix -- :
        refSelectFormat.current.selectedIndex=0
        // Signaler l'erreur :
        if(error.constructor.name == "Event") { // L'erreur est un événement : afficher son type
            errorMsg=t('saveAsImageButton.error_saving', "Error while saving picture : ")+error.type+" event"+"\n"+error
        } else {    // sinon, juste convertir l'objet en chaîne de caractères
            errorMsg=t('saveAsImageButton.error_saving', "Error while saving picture : ")+"\n"+error
        }
        console.error(errorMsg) // Affichage du message en tant qu'erreur (en rouge) dans la console des outils de dév. Web du navigateur
        console.log(error)  // Afficher l'objet JavaScript dans la console, pour pouvoir investiguer
        window.alert(errorMsg)  // Boîte de dialogue d'affichage pour l'utilisateur
    }

    // Gère la sélection d'un format d'image pour l'enregistrement
    const handleChange=(event) => {

        if(event.target.value != 'todo') {
            // Signaler que l'enregistrement est en cours : afficher le spinner
            refSpinner.current.style.visibility='visible'

            // Cherche la carte dans le DOM :
            const elements = document.getElementsByClassName("leaflet-container")
            if(elements.length > 0) {
                const mapElement=elements[0];   // Carte Leaflet

                // Taille de la carte, pour produire une image de mêmes dimensions
                const width=mapElement.clientWidth
                const height=mapElement.clientHeight

                // Enregistre l'image au format (type d'image) choisi :
                // Obligatoirement spécifier la taille !!! Voir : https://github.com/tsayen/dom-to-image#rendering-options
                switch(event.target.value) {
                    case "svg" :  // SVG
                        domtoimage.toSvg(mapElement, {width: width, height: height})
                            .then(saveImage)
                            .catch(onSaveImageError)
                        /*
                        try {
                            const image=saveSVG(mapElement, width, height)
                            saveImage(image)
                        }
                        catch(error) {
                            onSaveImageError(error)
                        }
                        */
                        break;
                    case "png": // PNG  
                        domtoimage.toPng(mapElement, {width: width, height: height})
                            .then(saveImage)
                            .catch(onSaveImageError)
                        break;
                    case "jpg": // JPG
                        domtoimage.toJpeg(mapElement, {width: width, height: height, quality: 0.95 })
                            .then(saveImage)
                            .catch(onSaveImageError)
                        break;
                }
            }                      
        }
        
    }

    // Affichage du composant : bouton avec icône + liste déroulante pour le choix du format d'enregistrement d'image (svg/png/jpg)
    return (<>
        <Col xs={"auto"} className={"ps-0"}>
            <Button 
                variant={selectFileFormat ? "secondary" : "outline-secondary"}
                className={selectFileFormat ? "" : "border border-1 shadow-none"} 
                title={t("saveAsImageButton.description", "Export map as image")} 
                onClick={() => {setSelectFileFormat(!selectFileFormat)}}
            >
                <FontAwesomeIcon icon={regular('file-image')} size="lg" />
            </Button>
        </Col>
        { selectFileFormat && <>
            <Col xs={"auto"} className={"ps-0"}>
                <Form.Select
                    onChange={handleChange} 
                    name={"img_format_select"} 
                    id={"dropdown-select-img-format"} 
                    aria-label={t('saveAsImageButton.select_img_format', "Select an image file format")} 
                    defaultValue={"todo"} 
                    ref={refSelectFormat} 
                >
                    <option value={"todo"} >-- {t('saveAsImageButton.select_img_format', "Select an image file format")} --</option>
                    <option value={"svg"} >{t('saveAsImageButton.svg_web_only', "SVG for the Web")}</option>
                    <option value={"png"} >PNG</option>
                    <option value={"jpg"} >JPG</option>
                </Form.Select>
            </Col>
            <Col xs={"auto"} className={"ps-0"} style={{visibility: 'hidden'}} ref={refSpinner} >
                <FontAwesomeIcon icon={solid("spinner")} spin pulse />
            </Col>
        </>}
    </>)
}
