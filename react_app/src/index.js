// index.js
/**
 * Point d'entrée de l'application React (pour Webpack : entry vizsources)
 */

import React from "react";
// import ReactDOM from "react-dom";    // ancienne API render jusqu'à React 17
import { createRoot } from 'react-dom/client';  // nouvelle API createRoot de React 18
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { VizSources } from './VizSources';
import './translate/i18nextInit';   // Détection langue du navigateur (ou préférence utilisateur) et initialisation syst. d'internationalisation
 

// Fix pb. d'image des marqueurs Leaflet avec Webpack :
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});
// Fix fin


// Insertion de l'application React dans la page Web :

// Cherche dans la page la div d'id 'render-react', dans laquelle insérer le rendu de l'application :
const domContainer = document.querySelector('#render-react');

// Insère la balise de l'application dans la div, en reprenant les paramètres data-xxx de la div du domContainer :
// ReactDOM.render(<VizSources {...domContainer.dataset} />, domContainer); // ancienne API render jusqu'à React 17

// Nouvelle API de React 18 pour la gestion de la racine du rendu :
const root = createRoot(domContainer);  // new root API. Also enables the new concurrent renderer. Concurrent rendering is interruptible (components behave slightly differently)
root.render(<VizSources {...domContainer.dataset} />);  // effectue le rendu de l'application, en reprenant les paramètres data-xxx (dataset) de la div du domContainer


/*
// Code de test de Leaflet en JavaScript : tuto à https://leafletjs.com/examples/quick-start/

import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'

var map = L.map('map').setView([51.505, -0.09], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
}).addTo(map);

// Fix pb. d'image des marqueurs Leaflet avec Webpack :
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});
// Fix fin

// Marqueurs :
var marker = L.marker([51.5, -0.09]).addTo(map);

var circle = L.circle([51.508, -0.11], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(map);

var polygon = L.polygon([
    [51.509, -0.08],
    [51.503, -0.06],
    [51.51, -0.047]
]).addTo(map);

// Popup :
marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
circle.bindPopup("I am a circle.");
polygon.bindPopup("I am a polygon.");

// ou

var popup = L.popup()
    .setLatLng([51.513, -0.09])
    .setContent("I am a standalone popup.")
    .openOn(map);

// Evénement avec Popup :

var popup = L.popup();

function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(map);
}

map.on('click', onMapClick);
*/
