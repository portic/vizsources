const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack'); // to access built-in plugins
const path = require('path');

module.exports = {
    entry: {    // Point(s) d'entrée de l'arbre de code de l'application. Voir "output" pour la (les) sortie(s) correspondante(s) dans /static/dist
        vizsources: "./react_app/src/index.js",    // Nom du module : name = vizsources
            // Toute ressource utilisée doit être importée depuis ce fichier, ou un de ses fichiers inclus (arborescence), pour être traitée vers dist
            // Voir aussi : https://webpack.js.org/concepts/entry-points
            // Webpack 4+ : 1 point d'entrée pour chaque page html de l'application (si plusieurs !). Les ressources partagées par les pages seront dans des bundles communs (voir https://webpack.js.org/configuration/optimization/#optimizationsplitchunks)
    },
    module: {
        rules: [    // Quel loader (use) gère quel type(s) de fichier(s) (test). (Sans les loaders, seuls les .js et .json sont gérés !)
            {
                test: /\.(js|jsx)$/,    // Code ES6 à transpiler avec Babel (gère la syntaxe React .jsx, et le chargement des seules icônes Fontawesome utilisées grâce à son plugin macros)
                use: "babel-loader",
            },
            {
                test: /\.(svg|png|jpg|jpeg|gif)$/i, // Images (ressources -> hash + transfert dans dist)
                type: 'asset/resource',
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,   // Polices de caractères (ressources)
                type: 'asset/resource',
            },
            {
                test: /\.css$/i,    // Feuilles de styles (traitées / intégrées au bundle + traitement des ressources incluses vers dist selon règles précédentes)
                use: ["style-loader", "css-loader"],
            },
            // Gère aussi le TypeScript (ts-loader), les styles sass (sass-loader), Markdown(/mdx)-to-html (markdown-loader / remark-loader, ex.: https://intoli.com/blog/webpack-markdown-setup/), ... 
            // (peuvent exploiter tout ce que Node.js peut faire : compression, packaging, language translations ...)
            // Le module Node correspondant au loader doit être installé (avec son module d'implémentation qui réalise la fonctionnalité)
            // Les loaders peuvent être chaînés (transformations appliquées dans l'ordre inverse de la chaîne) 
            //      ex. pour styles sass/fichiers .scss:    { test: /\.s[ac]ss$/i, use: ['style-loader', 'css-loader', 'sass-loader'] }    voir https://www.npmjs.com/package/sass-loader
            //
            // Voir : https://webpack.js.org/concepts/loaders/ et la doc des loaders : https://webpack.js.org/loaders/
        ],
    },
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx", ".json"], // extensions que Webpack ajoutera aux noms de fichiers pour essayer de les trouver
    },
    output: {   // Pour chaque point d'entrée défini dans "entry", génère un bundle dans le chemin défini, en reprenant le nom du point d'entrée :
        path: __dirname + "/static/dist",
        filename: "[name].bundle.js",
    },
    plugins: [
        new CleanWebpackPlugin(), // Nettoyage avant de construire : supprime tous les fichiers du répertoire dist sans pour autant supprimer ce dossier
      ]
};
