---
language: fr
title: Manuel utilisateur de Vizsources
summary: Notice d'aide à l'utilisation du logiciel Vizsources, permettant la visualisation des sources de Navigocorpus (projet ANR Portic)
authors:
    - Silvia Marzagalli
    - Bernard Pradines
date: 2023-03-07
copyright: © the ANR Portic Project 2019-2023. All rights reserved.
license: GNU Affero General Public License Version 3 (AGPL v3)
document_url: http://vizsources.portic.fr/static/manual/fr/usermanual.html
vizsources_url: http://vizsources.portic.fr/
portic_url: https://anr.portic.fr
---

# Introduction. A quoi sert cette visualisation ?

Cette visualisation permet de comprendre les sources qui ont été saisies dans la base de données et qui sont ensuite présentées dans les autres visualisations proposées par Portic, pour [suivre l'itinéraire d'un capitaine ou d'un navire précis](http://shiproutes.portic.fr/), ou pour avoir une [vision agrégée des itinéraires, pavillons, tonnages, etc.](http://explore1.portic.fr/)

Les sources mobilisées présentent des lacunes: bien que notre collecte des congés de 1787 ait été la plus exhaustive possible, certains registres manquent. Cette visualisation vise aussi à faire prendre conscience de l'ampleur des lacunes pour éviter des conclusions erronées lors de l'analyse des données.

- La [section 1](#1-comprendre-le-contenu-de-la-base-de-donnees-et-les-lacunes-dans-la-documentation) du manuel montre le degré de complétude des sources saisies.

- La [section 2](#2-comprendre-la-completude-des-variables) explique comment visualiser le degré de complétude de certaines variables importantes (port d'attache du navire, tonnage, etc.). 

- La [section 3](#3-regler-les-parametres-daffichage-de-la-carte-et-lexporter) montre comment régler les paramètres d'affichage de la carte, comment l'exporter, et précise les mentions obligatoires à citer en cas d'utilisation de cartes produites avec notre interface.

# 1. Comprendre le contenu de la base de données et les lacunes dans la documentation

Pour une description détaillée des corpus des sources mobilisées, se rapporter à   [la description des sources en ligne](https://anr.portic.fr/les-sources/).

## 1.1 Sélectionner la source archivistique

L'interface est réglée par défaut sur l'année 1787 et sur les sources issues de la sous-série G5 conservée aux Archives Nationales de France, c'est à dire sur les registres des congés. Il est possible de sélectionner les autres sources présentes dans la base en cliquant en haut à gauche dans le menu "Source". 

![Sélectionner la source archivistique](img/figure_1.jpg)  
***Figure 1** - Sélectionner la source archivistique*

La partie de la base Navigocorpus accessible via Portic comprend:

- La sous-série G5 des **congés (=départs)** délivrés en France en 1787 et, pour les ports des provinces de Flandre, Poitou, Aunis et Saintonge seulement, l'année 1789;
- Les registres des dépositions des capitaines du bureau de la Santé de Marseille liées au contrôle sur les patentes de Santé (ici abrégé: **Patentes de Marseille**) en 1749, 1759, 1769, 1779, 1787, 1789, 1799;
- Les registres des **entrées du petit cabotage** (navigation en Méditerranée nord-occidentale) de Marseille en 1787 et 1789;
- Les **départs au long cours depuis Marseille** en 1789
  
## 1.2 Sélectionner l'année

L'interface propose dans le menu central de sélectionner une des années pour lesquelles des données ont été saisies dans la base. Il suffit pour cela de cliquer sur une année dans la liste déroulante.

!!! warning "Attention"

    - Les années disponibles diffèrent en fonction de la source choisie. 
    - Il est impossible de cliquer sur des années dont aucune donnée n'a été saisies dans la base de données. 
    - Cf. § [1.1 Sélectionner la source archivistique](#11-selectionner-la-source-archivistique)

## 1.3 Qu'est-ce qui s'affiche?

Une fois la source et l'année choisies, la carte affiche les données présentes dans la base. 

!!! note "Comment lire les données qui s'affichent pour la variable de complétude « Nombre de départs ou d'entrées »"

    - Un cercle colorié bleu sur un port signifie que notre base des données comporte des enregistrements pour la source et l'année sélectionnées. Sa taille est proportionnelle au nombre de départs (ou d'entrées) observés.
    - Pour les congés de la sous-série G5, la carte indique aussi, avec un cercle proportionnel blanc, le nombre total des congés délivrés, même lorsque nous ne disposons pas des registres avec les données: ce total nous est connu grâce aux comptes rendus de la même sous-série.

En survolant une localité avec la souris, un tableau apparaît. Il permet d'avoir des renseignements plus précis sur le nombre de départs ou d'entrées fournis par la source sélectionnée, sur le tonnage total connu, et sur le statut du port. Les images suivantes permettent de comprendre comment lire ces tableaux.

![Comprendre le tableau en surbrillance qui s'affiche en survolant une localité avec la souris](img/figure_2.jpg)  
***Figure 2** - Comprendre le tableau en surbrillance qui s'affiche en survolant une localité avec la souris*

L'exemple des congés de Bordeaux (saisis pour  1787 mais pas pour 1789) permet de mieux comprendre les indications du tableau, en fonction de la présence ou pas des données dans la base. 

![Faire la différence entre les données présentes dans la base et les données connues par ailleurs](img/figure_3.jpg)  
***Figure 3** - Faire la différence entre les données présentes dans la base et les données connues par ailleurs*

Pour Marseille, les données proviennent des différentes sources. Le tableau suivant montre comment lire le tableau qui s'affiche en survolant ce port. 

<a id="figure_4"></a>
![Lire le tableau en surbrillance sur Marseille](img/figure_4.jpg)  
***Figure 4** - Lire le tableau en surbrillance sur Marseille*

# 2. Comprendre la complétude des variables

L'interface est réglée par défaut pour indiquer le nombre de départs (ou d'entrées) des navires indiqués par la source sélectionnée pour  l'année sélectionnée. 

Il est possible d'afficher d'autres variables et d'en visualiser le degré de complétude. La présence de certaines informations au sein de la même série dépend des habitudes locales: certaines informations ne sont pas systématiquement renseignées dans les registres.

!!! note "NOTA BENE"

    La connaissance de ces lacunes est fondamentale pour éviter des contresens. Ainsi, le tonnage des navires n'est pas une variable indiquée dans les registres du petit cabotage en 1787 (voir [figure 4](#figure_4)). Toutefois, nous connaissons le tonnage de certains bâtiments. Le total de 19835 tonneaux qui s'affiche dans le tableau en survol n'est donc pas le total du tonnage entré !

## 2.1 Sélectionner une variable pour voir sa complétude

Pour visualiser le degré de leur complétude, il est possible de sélectionner les variables suivantes en cliquant sur le menu déroulant de "Variable (complétude)":

![Sélectionner une variable pour voir sa complétude](img/figure_5.jpg)  
***Figure 5** - Sélectionner une variable pour voir sa complétude*

En fonction du choix de la variable, la carte affiche ce qui suit:

- **Tonnage**: la carte indique, par port, le degré de complétude de la variable "tonnage"
- **Port d'attache**: la carte indique, par port, le degré de complétude de la variable "port d'attache"
- **Produit ou but du voyage**: la carte indique, par port, le degré de complétude de la variable précisant les produits ou le but du voyage (ex.: "pour la pêche")
- **Identification locale du capitaine**: la carte indique, par port, le degré de complétude de l'indication d'appartenance géographique du capitaine (ex. "Jean Dupont d'Antibes")
- **Identification "nationale" du capitaine**: la carte indique, par port, le degré de complétude de l'indication d'appartenance plus large du capitaine (ex. "Catalan", "Grec", "génois", etc.)

## 2.2 Lire les données de complétude qui s'affichent 

Une fois la sélection de la variable effectuée, la carte montre pour chaque port, par un cercle de couleur, **le pourcentage des enregistrements** de la source et de l'année choisies **pour lesquels la variable sélectionnée est renseignée** (complétude). 

Les **classes de valeurs**, repérées par des couleurs, sont explicitées dans la légende de la carte, où elles sont suivies entre parenthèses du nombre (effectif) de ports concernés :  

![Classes de valeurs, dans la légende de la carte](img/figure_6a.jpg)  
***Figure 6a** - Classes de valeurs, dans la légende de la carte*

Il est toutefois possible d'obtenir **les pourcentages précis en survolant les ports** avec la souris. Un texte s'affiche alors en surbrillance. Il précise le nombre d'enregistrements, et entre parenthèses : d'abord *le pourcentage de complétude*, puis pour certaines variables, *la part déduite* de celui-ci qui provient de l'enrichissement des données de la base permis par notre travail d'identification des navires et des capitaines :

![Complétude (%) de la variable : port d'attache, pour Bastia, source : Congés (départs), année : 1787](img/figure_6b.jpg)  
***Figure 6b** - Complétude (%) de la variable : port d'attache, pour Bastia, source : Congés (départs), année : 1787*

Voici **comment les données sont enrichies**. Une fois attribué le même identifiant à deux navires, certaines informations présentes dans une source (par ex., le port d'attache ou le tonnage du navire) sont rapportées sur celle où elles manquent. Ces données déduites d'une autre source sont moins certaines, car elles dépendent de l'exactitude de notre identification des navires et des capitaines (plus sur ce sujet ici : [L'identification des navires](https://anr.portic.fr/incertitude/#Lidentification_des_navires)). 

![Lire la complétude d'une variable](img/figure_6c.jpg)  
***Figure 6c** - Lire la complétude d'une variable*

# 3. Régler les paramètres d'affichage de la carte et l'exporter

Une fois les sélections effectuées, il est possible d'exporter la carte en différents formats ou de créer une URL pérenne qui peut par exemple être citée dans une publication et qui renvoie automatiquement aux filtres séléctionnés. 

Cette section explique comment paramétrer la carte, puis comment l'exporter.

## 3.1 Régler les paramètres d'affichage de la carte 

### Zoomer et dézoomer 

L'utilisateur peut zoomer et dezoomer sur la carte en utilisant les symboles "+" et "-" en haut à gauche de la carte. De nouveaux toponymes s'affichent en zoomant.

![Modifier le zoom de la carte](img/figure_7.jpg)  
***Figure 7** - Modifier le zoom de la carte*

### Ajouter ou enlever l'affichage des frontières et/ou des toponymes des ports de la carte

Le fond de carte s'affiche par défaut sans frontières (trait de côte uniquement). Une ligne segmentée indique cependant les frontières entre deux amirautés. Les toponymes de ports présents dans la base s'affichent également, leur nombre dépend du niveau de zoom.

Vous avez la possibilité d'afficher la carte avec les frontières politiques de 1789 et, pour la France, avec aussi les frontières des provinces. 

Il est possible d'ajouter ou d'enlever :

- les frontières (monde en 1789)
- les limites d'amirautés
- les noms des ports

Pour cela, cliquez sur l'icône qui s'affiche au-dessus de la légende.

![Modifier l'affichage des frontières administratives, des limites d'amirautés et des toponymes](img/figure_8a.jpg)  
***Figure 8a** - Modifier l'affichage des frontières administratives, des limites d'amirautés et des toponymes*

![Exemples d'affichages en fonction de la sélection](img/figure_8b.jpg)  
***Figure 8b** - Exemples d'affichages en fonction de la sélection*

### Enlever le symbole +/- 

Il est possible, notamment en vue de l'exportation de la carte sélectionnée, d'enlever le symbole +/- qui permet de zoomer et dézoomer la carte. Pour ce faire, cocher l'option "Carte sans contrôles"

![Ne pas afficher le symbole pour zoomer](img/figure_9.jpg)  
***Figure 9** - Ne pas afficher le symbole pour zoomer*

## 3.2 Exporter la carte en tant qu'image

Pour exporter la carte en tant qu'image, il faut d'abord cliquer sur l'icone du menu "Exporter la carte en tant qu'image", puis séléctionner le format souhaité (SVG pour le Web, .PNG ou .JPG). En cliquant sur le format, le téléchargement de la carte commence. Vous la retrouvez dans votre dossier "téléchargements".

![Comment exporter la carte en tant qu'image](img/figure_10.jpg)  
***Figure 10** - Comment exporter la carte en tant qu'image*

## 3.3 Exporter la carte en tant que lien

Pour exporter le lien de votre carte en conservant les paramètres et les réglages que vous avez choisis, il suffit de cliquer sur l'icône 'Lien'. Le texte clignotant "fait" s'affiche à côté du symbole pendant 4 secondes : l'URL a été copiée dans votre presse-papier, vous n'avez plus alors qu'à la coller dans votre texte.

![Comment exporter la carte en tant que lien](img/figure_11.jpg)  
***Figure 11** - Comment exporter la carte en tant que lien*

## 3.4 Conditions d'utilisation des cartes et des liens exportés

Les cartes et les liens exportés sont mis à disposition selon les termes de la [Licence Creative Commons Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/deed.fr). Merci de consulter les termes de la licence avant utilisation.



!!! note "Mentions obligatoires à citer en cas d'utilisation de cartes produites avec notre interface :"

    [![Licence Creative Commons Attribution 4.0 International (CC BY 4.0)](https://i.creativecommons.org/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/deed.fr) Carte issue de [*VizSources*](http://vizsources.portic.fr/), une visualisation du [*Projet ANR Portic (2019-2023)*](https://anr.portic.fr/), sans (ou avec) modification(s). Mise à disposition selon les termes de la [Licence Creative Commons Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/deed.fr).    

