# VizSources : visualization of the sources from the Navigocorpus database

*Help Page for the vizsources app*

<!-- Admonitions (ci-après), voir https://python-markdown.github.io/extensions/admonition/ et https://squidfunk.github.io/mkdocs-material/reference/admonitions/ -->

!!! note "The visualization interface allows you to:"

    1. **Understand the sources** entered in the database and the gaps in the documentation,
    2. Know the **degree of completeness** of the main variables of the data inserted in the database.

For more info, see the [user manual](usermanual.md)

You can also [try the app online](http://vizsources.portic.fr/)

Dynamic data provided by [the Portic API](http://data.portic.fr/)

Other visualizations from the ANR Portic project : [https://anr.portic.fr/acces-visualisations/](https://anr.portic.fr/acces-visualisations/)

VizSources is an open-source Web application, distributed under the license : [![AGPLv3.png](http://www.gnu.org/graphics/agplv3-with-text-162x68.png)](agpl-3.0.md)

This is the **user documentation** for *VizSources*, a visualization of the *ANR Portic* project: [![https://anr.portic.fr/](img/logo_portic_petit_100.jpg)](https://anr.portic.fr/)