# VizSources : visualisation des sources de Navigocorpus

*Page d'Aide de l'application vizsources*

<!-- Admonitions (ci-après), voir https://python-markdown.github.io/extensions/admonition/ et https://squidfunk.github.io/mkdocs-material/reference/admonitions/ -->

!!! note "Cette interface de visualisation permet de:"

    1. **Comprendre les sources** saisies dans la base et les lacunes dans la documentation,
    2. Connaître le **degré de complétude** des principales variables des données insérées dans la base.

Consultez le [manuel utilisateur](usermanual.md) pour plus d'informations

Vous pouvez aussi [voir l'application en ligne](http://vizsources.portic.fr/)

Données dynamiques fournies par [l'API de Portic](http://data.portic.fr/)

Autres visualisations du projet ANR Portic : [https://anr.portic.fr/acces-visualisations/](https://anr.portic.fr/acces-visualisations/)

VizSources est une application Web open-source, distribuée sous licence : [![AGPLv3.png](http://www.gnu.org/graphics/agplv3-with-text-162x68.png)](agpl-3.0.md)

Ceci est la **documentation utilisateur** de *VizSources*, une visualisation du projet *ANR Portic* : [![https://anr.portic.fr/](img/logo_portic_petit_100.jpg)](https://anr.portic.fr/)