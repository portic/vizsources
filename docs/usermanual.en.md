---
language: en
title: Vizsources User Manual
summary: Help guide for using the Vizsources software, allowing the visualization of Navigocorpus sources (ANR Portic project)
authors:
    - Silvia Marzagalli
    - Bernard Pradines
date: 2023-03-07
copyright: © the ANR Portic Project 2019-2023. All rights reserved.
license: GNU Affero General Public License Version 3 (AGPL v3)
document_url: http://vizsources.portic.fr/static/manual/en/usermanual.html
vizsources_url: http://vizsources.portic.fr/
portic_url: https://anr.portic.fr
---

# Introduction. What is this visualization for?

This visualization makes it possible to understand the sources that have been entered in the database and which are then presented in the other visualizations offered by Portic, to [follow the route of a specific captain or ship](http://shiproutes.portic.fr/), or to get an [aggregate view of routes, flags, tonnages, etc.](http://explore1.portic.fr/)

The sources used have gaps: although our collection of clearances for year 1787 was as exhaustive as possible, some registers are missing. This visualization also aims to raise awareness of the extent of the gaps to avoid erroneous conclusions when analyzing the data.

- [Section 1](#1-understanding-database-content-and-documentation-gaps) of the manual shows the degree of completeness of the sources entered.

- [Section 2](#2-understanding-variable-completeness) explains how to visualize the degree of completeness of certain important variables (ship's home port, tonnage, etc.).

- [Section 3](#3-adjust-map-display-settings-and-export) shows how to set the parameters of the map, how to export it, and specifies the mandatory information to quote when using maps produced with our interface.

# 1. Understanding database content and documentation gaps

For a detailed description of the corpus of the sources used, refer to [the description of online sources](https://anr.portic.fr/les-sources/).

## 1.1 Select archival source

The interface is set by default to the year 1787 and to the sources from the G5 sub-series kept at the National Archives of France, ie the clearances registers. It is possible to select the other sources present in the database by clicking on the top left in the "Source" menu.

![Select archival source](img/figure_1.jpg)  
***Figure 1** - Select archival source*

The part of the Navigocorpus database accessible via Portic includes:

- The sub-series of **Clearances** (=departures, leaves) issued in France in 1787 and, for the ports of the provinces of Flanders, Poitou, Aunis and Saintonge only, the year 1789;
- The registers of depositions of the captains at the Office of Health in Marseille related to the control on the health patents (here abbreviated: **Bill of Health in Marseille** - entrances) in 1749, 1759, 1769, 1779, 1787, 1789, 1799;
- The registers of the **Entrances in Marseille** of the small cabotage (navigation in the north-western Mediterranean) in 1787 and 1789;
- **Clearances from Marseille** (Long course departures, to America and Indian ocean) in 1789
  
## 1.2 Select year

In the central menu of the interface, you can select one of the years for which data has been entered in the database. Just click on a year in the dropdown list.

!!! warning "Attention"

    - The years available differ depending on the source chosen. 
    - It is not possible to click on years for which no data has been entered into the database. 
    - See § [1.1 Select archival source](#11-select-archival-source)


## 1.3 What is displayed?

Once the source and the year have been chosen, the map displays the data present in the database. 

!!! note "How to read the data displayed for the "Number of departures or entrances" completeness variable"

    - A blue colored circle on a port means that our database has records for the selected source and year. Its size is proportional to the number of departures (or entrances) observed.
    - For clearances of the G5 sub-series, the map also indicates, with a white proportional circle, the total number of clearances issued, even when we do not have the registers with the data: this total is known to us thanks to the reports of the same subseries.

By hovering over a locality with the mouse, a table appears. It provides more precise information on the number of departures or entrances provided by the selected source, on the known total tonnage, and on the status of the port. The following images help to understand how to read these tables.

![Understand the highlighted table that appears when hovering over a locality with the mouse](img/figure_2.jpg)  
***Figure 2** - Understand the highlighted table that appears when hovering over a locality with the mouse*

The example of the clearances in Bordeaux (entered for 1787 but not for 1789) makes it possible to better understand the indications of the table, depending on the presence or not of the data in the database. 

![Differentiate between the data present in the database and the data known elsewhere](img/figure_3.jpg)  
***Figure 3** - Differentiate between the data present in the database and the data known elsewhere*

For Marseille, the data comes from the different sources. The following table shows how to read the table displayed by hovering over this port. 

<a id="figure_4"></a>
![Read the highlighted table for Marseille](img/figure_4.jpg)  
***Figure 4** - Read the highlighted table for Marseille*

# 2. Understanding variable completeness

The interface is set by default to indicate the number of departures (or entrances) of the vessels indicated by the selected source for the selected year. 

It is possible to display other variables and view their degree of completeness. The presence of certain information within the same series depends on local habits: certain information is not systematically entered in the registers.

!!! note "NOTA BENE"

    Knowledge of these shortcomings is fundamental to avoid misinterpretations. Thus, the tonnage of ships is not a variable indicated in the costal trade registers in 1787 (see [figure 4](#figure_4)). However, we do know the tonnage of certain vessels. The total of 19835 barrels that is displayed in the hover table is therefore not the total tonnage entered!

## 2.1 Select a variable to see its completeness

To view the degree of their completeness, it is possible to select the following variables by clicking on the drop-down menu of "Variable (completeness)":

![Select a variable to see its completeness](img/figure_5.jpg)  
***Figure 5** - Select a variable to see its completeness*

Depending on the choice of variable, the map displays the following:

- **Ship burthen**: the map indicates, by port, the degree of completeness of the "Ship burthen" (tonnage) variable
- **Ship's homeport**: the map indicates, by port, the degree of completeness of the "Ship's homeport" (home port) variable
- **Cargo or scope of voyage**: the map indicates, by port, the degree of completeness of the variable specifying the products or the purpose of the voyage (e.g.: "for fishing")
- **Captain local identification**: the map indicates, by port, the degree of completeness of the indication of the geographical affiliation of the captain (eg "Jean Dupont d'Antibes")
- **Captain "national" identification**: the map indicates, by port, the degree of completeness of the broader indication of membership of the captain (ex. "Catalan", "Greek", "Genoese", etc.)

## 2.2 Read the completeness data that is displayed 

Once the variable has been selected, the map shows for each port, with a colored circle, **the percentage of records** of the chosen source and year **for which the selected variable is filled in** (completeness).

The **classes of values**, identified by colors, are explained in the legend of the map, where they are followed in parentheses by the number of ports concerned:

![Value classes, in the map legend](img/figure_6a.jpg)  
***Figure 6a** - Value classes, in the map legend*

However, it is possible to obtain **the precise percentages by hovering over the ports** with the mouse. A text is then highlighted. It specifies the number of records, and in parentheses: first *the percentage of completeness*, then for certain variables, *the part deducted* from it which comes from the enrichment of the database data allowed by our vessel and captain identification work:

![Completeness (%) of the variable: Ship's homeport, for Bastia, source: Clearances (departures), year: 1787](img/figure_6b.jpg)  
***Figure 6b** - Completeness (%) of the variable: Ship's homeport, for Bastia, source: Clearances (departures), year: 1787*

This is **how the data is enriched**. Once the same identifier has been assigned to two vessels, certain information present in one source (eg the home port or the tonnage of the vessel) is reported on the one where it is missing. These data inferred from another source are less certain, as they depend on the accuracy of our identification of ships and captains (more on this here: [Vessel identification](https://anr.portic.fr/incertitude/#Lidentification_des_navires)). 

![Read the completeness of a variable](img/figure_6c.jpg)  
***Figure 6c** - Read the completeness of a variable*

# 3. Adjust map display settings and export

Once the selections have been made, it is possible to export the map in different formats or to create a permanent URL which can for example be quoted in a publication and which automatically refers to the selected filters.

This section explains how to configure the map, then how to export it.

## 3.1 Adjust map display settings 

### Zoom in and zoom out 

The user can zoom in and out of the map using the "+" and "-" symbols at the top left of the map. New toponyms are displayed by zooming.

![Change map zoom level](img/figure_7.jpg)  
***Figure 7** - Change map zoom level*

### Toggle on or off the display of borders and/or toponyms of ports on the map

The basemap is displayed by default without borders (coastline only). A segmented line, however, indicates the limit between two admiralties. The toponyms of ports present in the database are also displayed, their number depends on the zoom level.

You can choose to display the basemap with the political borders of 1789 and, for France, also with the borders of the provinces.

It is possible to add or remove:

- borders (world in 1789)
- admiralty limits
- port names

To do so, click on the icon that appears above the legend.

![Toggle the display of administrative borders, admiralty limits and toponyms](img/figure_8a.jpg)  
***Figure 8a** - Toggle the display of administrative borders, admiralty limits and toponyms*

![Examples of displays depending on the selection](img/figure_8b.jpg)  
***Figure 8b** - Examples of displays depending on the selection*

### Remove +/- symbol 

If you want to export the current map, you may wish to remove the +/- symbol (which allows the map to be zoomed in and out). To do so, check the checkbox labeled "Map with no control"

![Don't show symbol to zoom](img/figure_9.jpg)  
***Figure 9** - Don't show symbol to zoom*

## 3.2 Export map as image

To export the map as an image, you must first click on the menu icon "Export map as image", then select the desired image file format (SVG for the Web, .PNG or .JPG). By clicking on the format, the download of the map begins. You find it in your "Downloads" folder.

![How to export the map as an image](img/figure_10.jpg)  
***Figure 10** - How to export the map as an image*

## 3.3 Export map as link

To export the link of your map while keeping the parameters and settings you have chosen, just click on the 'Link' icon. The blinking text "done" appears next to the symbol for 4 seconds: the URL has been copied to your clipboard, you just have to paste it into your text.

![How to export the map as a link](img/figure_11.jpg)  
***Figure 11** - How to export the map as a link*

## 3.4 Terms of use for exported maps and links

Exported maps and links are licensed under a [Creative Commons Attribution 4.0 International License (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/deed.en). Please read the license terms before use.



!!! note "Mandatory information to quote when using maps produced with our interface:"

    [![Creative Commons Attribution 4.0 International License (CC BY 4.0)](https://i.creativecommons.org/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/deed.en) Map from [*VizSources*](http://vizsources.portic.fr/), a visualization of  [*The ANR Portic Project (2019-2023)*](https://anr.portic.fr/), with (or without) change(s). Licensed under a [Creative Commons Attribution 4.0 International License (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/deed.en).    

